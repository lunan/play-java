name := """play-java"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  filters,
  javaJdbc,
  javaWs,
  cache,
  javaWs,
  javaJpa,
  "org.apache.activemq" % "activemq-client" % "5.8.0",
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "com.auth0" % "java-jwt" % "2.2.0"
)



fork in run := false
offline:=false

javaOptions in run +="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=15002"