package controllers.rest;

import javax.inject.Inject;

import models.ApplicationIdGenerator;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {
	
	@Inject
	ApplicationIdGenerator appIdGenerator;
	
	@Transactional
	public Result getApplicationId(){
		String appId = appIdGenerator.getApplicationId();
		if(appId != null){
			return ok(appId);
		} else {
			return internalServerError();
		}
	}
}
