package controllers.rest;

import javax.inject.Inject;

import models.DDEJSONModel;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.external.DDEWorkflowRESTConnector;
import services.user.DDEService;

public class DetailDataEntryController extends BaseController {
	DDEService crudService;
	DDEJSONModel jsonModel;
	@Inject DDEWorkflowRESTConnector connector;
	
	
	@Inject
	public DetailDataEntryController(DDEService crudService, DDEJSONModel jsonModel) {
		super(crudService, jsonModel);
		this.crudService=crudService;
		this.jsonModel=jsonModel;
	}

	@BodyParser.Of(BodyParser.Json.class)
	@Transactional
	@Override
	public Result save() {
		try{
			DDEJSONModel newModel = Json.fromJson(request().body().asJson(), jsonModel.getClass());
			crudService.save(newModel);
			connector.init(newModel);
			return ok();
		} catch(Exception e)  {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	
}
