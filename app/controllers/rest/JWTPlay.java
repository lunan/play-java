package controllers.rest;

import java.util.HashMap;
import java.util.Map;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;

import play.mvc.Controller;
import play.mvc.Result;

public class JWTPlay extends Controller {
	
	final String secret = "this is my secret";
	final String issuer = "BANK_SYARIAH_MANDIRI";
	final JWTSigner signer = new JWTSigner(secret);

	public Result generateJWT(){
		Map<String, Object> claims = new HashMap<>();
		claims.put("iss", issuer);
		return ok(signer.sign(claims));
	}
	
	public Result verifyJWT(){
		String authHeader = request().getHeader("Authorization");
    	JWTVerifier verifier = new JWTVerifier(secret);
    	try {
			
			Map<String,Object> claims = verifier.verify(authHeader);
			return ok(claims.get("iss").toString());
		} catch (Exception e){
			e.printStackTrace();
			return badRequest(e.getMessage());
		}
	}
}
