package controllers.rest;

import javax.inject.Inject;

import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.AddressService;

public class AddressUtil extends Controller {

	@Inject AddressService addressService;
	
	@Transactional
	public Result getAllPropinsi(){
		return ok(Json.toJson(this.addressService.getAllPropinsi()));
	}
	
	@Transactional
	public Result getKabupatenKota(String propinsi){
		return ok(Json.toJson(this.addressService.getKabupatenKota(propinsi)));
	}
	
	@Transactional
	public Result getKecamatan(String propinsi, String kotaKabupaten){
		return ok(Json.toJson(this.addressService.getKecamatan(propinsi, kotaKabupaten)));
	}
	
	@Transactional
	public Result getKelurahan(String propinsi, String kotaKabupaten,String kecamatan){
		return ok(Json.toJson(this.addressService.getKelurahan(propinsi, kotaKabupaten, kecamatan)));
	}
}
