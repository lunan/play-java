package controllers.rest;

import javax.inject.Inject;

import models.*;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.external.IDEWorkflowRESTConnector;
import services.user.IDEService;

public class InitialDataEntryController extends BaseController {

	@Inject IDEWorkflowRESTConnector connector;
	
	@Inject
	public InitialDataEntryController(IDEService crudService, IDEJSONModel jsonModel) {
		super(crudService, jsonModel);
	}
	
	/**
	 * Call workflow URL to initiate workflow
	 * TODO Change into Artemis JMS implementation
	 */
	@BodyParser.Of(BodyParser.Json.class)
	@Transactional
	@Override
	public Result save(){
		try{
			JSONModel newModel = Json.fromJson(request().body().asJson(), this.jsonModel.getClass());
			crudService.save(newModel);
			connector.init((IDEJSONModel) newModel);
			return ok();
		} catch(Exception e)  {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	
}
