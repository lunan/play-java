package controllers.rest;

import models.JSONModel;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.CRUDService;

public class BaseController extends Controller {
	
	protected CRUDService crudService = null;
	protected JSONModel jsonModel;
	
	public BaseController(CRUDService crudService, JSONModel jsonModel) {
		this.crudService = crudService;
		this.jsonModel=jsonModel;
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	@Transactional
	public Result save(){
		try{
			JSONModel newModel = Json.fromJson(request().body().asJson(), jsonModel.getClass());
			crudService.save(newModel);
			return ok();
		} catch(Exception e)  {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	@Transactional
	public Result update(String id){
		try{
			JSONModel newModel = Json.fromJson(request().body().asJson(), jsonModel.getClass());
			crudService.update(id, newModel);
			return ok();
		} catch(Exception e)  {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	public Result getSkeleton(){
		try{
			return ok(Json.toJson(crudService.getSkeleton()));
		} catch(Exception e)  {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	@Transactional
	public Result getall(){
		try {
			return ok(Json.toJson(crudService.getall()));
		} catch (Exception e) {
			e.printStackTrace();
			return notFound(e.getMessage());
		}
	}
	
	@Transactional
	public Result get(String id){
		try {
			return ok(Json.toJson(crudService.get(id)));
		} catch (Exception e) {
			e.printStackTrace();
			return notFound(e.getMessage());
		}
	}
	
	@Transactional
	public Result delete(String id){
		try {
			crudService.delete(id);
			return ok();
		} catch (Exception e) {
			e.printStackTrace();
			return notFound(e.getMessage());
		}
	}
	
}
