package controllers.rest;

import javax.inject.Inject;

import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.DocumentCheckService;

public class DocCheckUtil extends Controller {

	@Inject DocumentCheckService docCheckService;
	
	@Transactional
	public Result getCategories(){
		return ok(Json.toJson(this.docCheckService.getCategories() ));
	}
	
	@Transactional
	public Result getSubCategories(String category){
		return ok(Json.toJson(this.docCheckService.getSubCategories(category) ));
	}
	
	@Transactional
	public Result getDocNames(String category, String subCategory){
		return ok(Json.toJson(this.docCheckService.getDocNames(category, subCategory) ));
	}
}
