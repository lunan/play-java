package controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import play.*;
import play.mvc.Controller;
import services.ActiveMQConsumer;
import services.ActiveMQProducer;
import play.mvc.*;
import views.html.*;
import play.db.jpa.Transactional;

@Singleton
public class ActiveMQTestController extends Controller {
	
	private final ActiveMQProducer producer;
	private final ActiveMQConsumer consumer;
	
	@Inject
	public ActiveMQTestController(ActiveMQProducer producer, ActiveMQConsumer consumer) {
		this.producer = producer;
		this.consumer = consumer;
	}
	
	@Transactional
	public Result send(){
		try {
			producer.Send("HI ActiveMQ");
			return ok("SEND TESTING SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return ok("SEND TESTING ERROR");
		}
	}
	
	@Transactional
	public Result receive(){
		try {
			consumer.ConsumeMessage();
			return ok("RECEIVE TESTING SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			return ok("RECEIVE TESTING ERROR");
		}
	}
}
