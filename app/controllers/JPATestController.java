package controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import models.LookupBranch;
import play.db.jpa.Transactional;
import play.mvc.*;
import views.html.*;
import services.LookupBranchService;

@Singleton
public class JPATestController extends Controller {

	private final LookupBranchService branchService;
	
	@Inject
	public JPATestController(LookupBranchService branchService) {
		this.branchService=branchService;
	}
	
	@Transactional
	public Result save(){
		LookupBranch branch = branchService.find("ID0010004");
		return ok();
	}
	
	
}
