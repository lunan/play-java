package models.transformer;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimestampTransformer {
	public static String timestampToString(Timestamp tm){
		if(tm != null){
			return tm.toLocalDateTime().toLocalDate().format(DateTimeFormatter.ISO_DATE);
		} else {
			return "";
		}
	}
	
	public static Timestamp stringToTimestamp(String dt){
		if(dt != null && !dt.equals("")) {
			return Timestamp.valueOf(LocalDate.parse(dt, DateTimeFormatter.ISO_DATE).atStartOfDay());
		} else {
			return null;
		}
	}
	
	public static Timestamp getNow(){
		return Timestamp.valueOf(LocalDateTime.now());
	}
	
}
