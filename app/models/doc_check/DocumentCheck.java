package models.doc_check;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ORIG_DOC_CHECK database table.
 * 
 */
@Entity
@Table(name="ORIG_DOC_CHECK")
@NamedQuery(name="DocumentCheck.findAll", query="SELECT d FROM DocumentCheck d")
public class DocumentCheck implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"ID\"", insertable=false, updatable=false)
	private int id;

	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="LIST_ID")
	private int listId;

	@Column(name="\"VALUE\"")
	private String value;

	public DocumentCheck() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public int getListId() {
		return this.listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}