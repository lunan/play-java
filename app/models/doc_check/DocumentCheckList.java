package models.doc_check;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ORIG_DOC_CHECK_LIST database table.
 * 
 */
@Entity
@Table(name="ORIG_DOC_CHECK_LIST")
@NamedQuery(name="DocumentCheckList.findAll", query="SELECT o FROM DocumentCheckList o")
public class DocumentCheckList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String GET_CATEGORY = "SELECT DISTINCT CATEGORY FROM WISE.ORIG_DOC_CHECK_LIST ORDER BY CATEGORY";
	public static final String GET_SUB_CATEGORY = "SELECT DISTINCT SUB_CATEGORY FROM WISE.ORIG_DOC_CHECK_LIST WHERE CATEGORY = :category ORDER BY SUB_CATEGORY";
	public static final String GET_DOC_NAME = "SELECT ID,DOC_NAME,DEFAULT_VALUE FROM WISE.ORIG_DOC_CHECK_LIST WHERE CATEGORY = :category AND SUB_CATEGORY = :subCategory ORDER BY DOC_NAME";
	

	@Id
	@Column(name="\"ID\"", insertable=false, updatable=false)
	private int id;

	private String category;

	@Column(name="DOC_NAME")
	private String docName;

	@Column(name="DEFAULT_VALUE")
	private String defaultValue;

	@Column(name="SUB_CATEGORY")
	private String subCategory;

	public DocumentCheckList() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDocName() {
		return this.docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getSubCategory() {
		return this.subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

}