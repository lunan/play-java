package models;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import models.dde.DDEApplication;
import models.dde.DDECollBond;
import models.dde.DDECollCash;
import models.dde.DDECollGold;
import models.dde.DDECollLand;
import models.dde.DDECollLnb;
import models.dde.DDECollMachine;
import models.dde.DDECollSk;
import models.dde.DDECollVehicle;
import models.dde.DDECollateral;
import models.dde.DDECustWealthVehicle;
import models.dde.DDECustomer;
import models.dde.DDECustomerEmergency;
import models.dde.DDECustomerSpouse;
import models.dde.DDECustomerSpouseWork;
import models.dde.DDECustomerWork;
import models.dde.DDEFinanceBsmLoan;
import models.dde.DDEFinanceCc;
import models.dde.DDEFinanceLoan;
import models.dde.DDEKeyPerson;
import models.dde.DDELoanRequest;
import models.dde.DDEUploadDocument;
import models.dde.DDEWealthAcct;
import models.dde.DDEWealthProperty;

public class DDEJSONModel extends JSONModel {
	@JsonUnwrapped(prefix = "DDELoanRequest_")
	private DDELoanRequest ddeLoanRequest;

	@JsonUnwrapped(prefix = "DDECustomerWork_")
	private DDECustomerWork ddeCustomerWork;

	@JsonUnwrapped(prefix = "DDECollSk_")
	private DDECollSk ddeCollSk;

	@JsonUnwrapped(prefix = "DDECollateral_")
	private DDECollateral ddeCollateral;

	@JsonUnwrapped(prefix = "DDECustomerSpouseWork_")
	private DDECustomerSpouseWork ddeCustomerSpouseWork;

	@JsonUnwrapped(prefix = "DDECollCash_")
	private DDECollCash ddeCollCash;

	@JsonUnwrapped(prefix = "DDECustomerEmergency_")
	private DDECustomerEmergency ddeCustomerEmergency;

	@JsonUnwrapped(prefix = "DDECollLnb_")
	private DDECollLnb ddeCollLnb;

	@JsonUnwrapped(prefix = "DDECollGold_")
	private DDECollGold ddeCollGold;

	@JsonUnwrapped(prefix = "DDECustWealthVehicle _")
	private DDECustWealthVehicle ddeCustWealthVehicle;

	@JsonUnwrapped(prefix = "DDECollMachine_")
	private DDECollMachine ddeCollMachine;

	@JsonUnwrapped(prefix = "DDECollBond_")
	private DDECollBond ddeCollBond;

	@JsonUnwrapped(prefix = "DDEFinanceCc_")
	private DDEFinanceCc ddeFinanceCc;

	@JsonUnwrapped(prefix = "DDEApplication_")
	private DDEApplication ddeApplication;

	@JsonUnwrapped(prefix = "DDEFinanceBsmLoan_")
	private DDEFinanceBsmLoan ddeFinanceBsmLoan;

	@JsonUnwrapped(prefix = "DDEFinanceLoan_")
	private DDEFinanceLoan ddeFinanceLoan;

	@JsonUnwrapped(prefix = "DDEWealthAcct_")
	private DDEWealthAcct ddeWealthAcct;

	@JsonUnwrapped(prefix = "DDECollLand_")
	private DDECollLand ddeCollLand;

	@JsonUnwrapped(prefix = "DDEWealthProperty_")
	private DDEWealthProperty ddeWealthProperty;

	@JsonUnwrapped(prefix = "DDECollVehicle_")
	private DDECollVehicle ddeCollVehicle;

	@JsonUnwrapped(prefix = "DDECustomerSpouse_")
	private DDECustomerSpouse ddeCustomerSpouse;

	@JsonUnwrapped(prefix = "DDECustomer_")
	private DDECustomer ddeCustomer;

	@JsonUnwrapped(prefix = "DDEKeyPerson_")
	private DDEKeyPerson ddeKeyPerson;

	@JsonUnwrapped(prefix = "DDEUploadDocument_")
	private DDEUploadDocument ddeUploadDocument;

	public DDEJSONModel() {

	}

	public static final DDEJSONModel getInstance() {
		DDEJSONModel model = new DDEJSONModel();
		model.setDdeApplication(new DDEApplication());
		model.setDdeCollBond(new DDECollBond());
		model.setDdeCollCash(new DDECollCash());
		model.setDdeCollGold(new DDECollGold());
		model.setDdeCollLand(new DDECollLand());
		model.setDdeCollLnb(new DDECollLnb());
		model.setDdeCollMachine(new DDECollMachine());
		model.setDdeCollSk(new DDECollSk());
		model.setDdeCollVehicle(new DDECollVehicle());
		model.setDdeCollateral(new DDECollateral());
		model.setDdeCustWealthVehicle(new DDECustWealthVehicle());
		model.setDdeCustomer(new DDECustomer());
		model.setDdeCustomerEmergency(new DDECustomerEmergency());
		model.setDdeCustomerSpouse(new DDECustomerSpouse());
		model.setDdeCustomerSpouseWork(new DDECustomerSpouseWork());
		model.setDdeCustomerWork(new DDECustomerWork());
		model.setDdeFinanceBsmLoan(new DDEFinanceBsmLoan());
		model.setDdeFinanceCc(new DDEFinanceCc());
		model.setDdeFinanceLoan(new DDEFinanceLoan());
		model.setDdeKeyPerson(new DDEKeyPerson());
		model.setDdeLoanRequest(new DDELoanRequest());
		model.setDdeUploadDocument(new DDEUploadDocument());
		model.setDdeWealthAcct(new DDEWealthAcct());
		model.setDdeWealthProperty(new DDEWealthProperty());
		return model;
	}
	
	public void copyId(String id) {
		List<Field> fields = Arrays.asList(this.getClass().getDeclaredFields());
		for (Field field : fields) {
			if(!field.getName().equals("ddeApplication")){
				field.setAccessible(true);
				Object fValue;
				try {
					fValue = field.get(this);
					Method method = fValue.getClass().getDeclaredMethod("setApplicationId", String.class);
					method.invoke(fValue, id);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<?> allFields(){
		List<Object> allField = new ArrayList<>();
		List<Field> fields = Arrays.asList(this.getClass().getDeclaredFields());
		for (Field field : fields) {
			field.setAccessible(true);			
			try {
				allField.add(field.get(this));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return allField;
	}

	public DDELoanRequest getDdeLoanRequest() {
		return ddeLoanRequest;
	}

	public void setDdeLoanRequest(DDELoanRequest ddeLoanRequest) {
		this.ddeLoanRequest = ddeLoanRequest;
	}

	public DDECustomerWork getDdeCustomerWork() {
		return ddeCustomerWork;
	}

	public void setDdeCustomerWork(DDECustomerWork ddeCustomerWork) {
		this.ddeCustomerWork = ddeCustomerWork;
	}

	public DDECollSk getDdeCollSk() {
		return ddeCollSk;
	}

	public void setDdeCollSk(DDECollSk ddeCollSk) {
		this.ddeCollSk = ddeCollSk;
	}

	public DDECollateral getDdeCollateral() {
		return ddeCollateral;
	}

	public void setDdeCollateral(DDECollateral ddeCollateral) {
		this.ddeCollateral = ddeCollateral;
	}

	public DDECustomerSpouseWork getDdeCustomerSpouseWork() {
		return ddeCustomerSpouseWork;
	}

	public void setDdeCustomerSpouseWork(
			DDECustomerSpouseWork ddeCustomerSpouseWork) {
		this.ddeCustomerSpouseWork = ddeCustomerSpouseWork;
	}

	public DDECollCash getDdeCollCash() {
		return ddeCollCash;
	}

	public void setDdeCollCash(DDECollCash ddeCollCash) {
		this.ddeCollCash = ddeCollCash;
	}

	public DDECustomerEmergency getDdeCustomerEmergency() {
		return ddeCustomerEmergency;
	}

	public void setDdeCustomerEmergency(
			DDECustomerEmergency ddeCustomerEmergency) {
		this.ddeCustomerEmergency = ddeCustomerEmergency;
	}

	public DDECollLnb getDdeCollLnb() {
		return ddeCollLnb;
	}

	public void setDdeCollLnb(DDECollLnb ddeCollLnb) {
		this.ddeCollLnb = ddeCollLnb;
	}

	public DDECollGold getDdeCollGold() {
		return ddeCollGold;
	}

	public void setDdeCollGold(DDECollGold ddeCollGold) {
		this.ddeCollGold = ddeCollGold;
	}

	public DDECustWealthVehicle getDdeCustWealthVehicle() {
		return ddeCustWealthVehicle;
	}

	public void setDdeCustWealthVehicle(
			DDECustWealthVehicle ddeCustWealthVehicle) {
		this.ddeCustWealthVehicle = ddeCustWealthVehicle;
	}

	public DDECollMachine getDdeCollMachine() {
		return ddeCollMachine;
	}

	public void setDdeCollMachine(DDECollMachine ddeCollMachine) {
		this.ddeCollMachine = ddeCollMachine;
	}

	public DDECollBond getDdeCollBond() {
		return ddeCollBond;
	}

	public void setDdeCollBond(DDECollBond ddeCollBond) {
		this.ddeCollBond = ddeCollBond;
	}

	public DDEFinanceCc getDdeFinanceCc() {
		return ddeFinanceCc;
	}

	public void setDdeFinanceCc(DDEFinanceCc ddeFinanceCc) {
		this.ddeFinanceCc = ddeFinanceCc;
	}

	public DDEApplication getDdeApplication() {
		return ddeApplication;
	}

	public void setDdeApplication(DDEApplication ddeApplication) {
		this.ddeApplication = ddeApplication;
	}

	public DDEFinanceBsmLoan getDdeFinanceBsmLoan() {
		return ddeFinanceBsmLoan;
	}

	public void setDdeFinanceBsmLoan(DDEFinanceBsmLoan ddeFinanceBsmLoan) {
		this.ddeFinanceBsmLoan = ddeFinanceBsmLoan;
	}

	public DDEFinanceLoan getDdeFinanceLoan() {
		return ddeFinanceLoan;
	}

	public void setDdeFinanceLoan(DDEFinanceLoan ddeFinanceLoan) {
		this.ddeFinanceLoan = ddeFinanceLoan;
	}

	public DDEWealthAcct getDdeWealthAcct() {
		return ddeWealthAcct;
	}

	public void setDdeWealthAcct(DDEWealthAcct ddeWealthAcct) {
		this.ddeWealthAcct = ddeWealthAcct;
	}

	public DDECollLand getDdeCollLand() {
		return ddeCollLand;
	}

	public void setDdeCollLand(DDECollLand ddeCollLand) {
		this.ddeCollLand = ddeCollLand;
	}

	public DDEWealthProperty getDdeWealthProperty() {
		return ddeWealthProperty;
	}

	public void setDdeWealthProperty(DDEWealthProperty ddeWealthProperty) {
		this.ddeWealthProperty = ddeWealthProperty;
	}

	public DDECollVehicle getDdeCollVehicle() {
		return ddeCollVehicle;
	}

	public void setDdeCollVehicle(DDECollVehicle ddeCollVehicle) {
		this.ddeCollVehicle = ddeCollVehicle;
	}

	public DDECustomerSpouse getDdeCustomerSpouse() {
		return ddeCustomerSpouse;
	}

	public void setDdeCustomerSpouse(DDECustomerSpouse ddeCustomerSpouse) {
		this.ddeCustomerSpouse = ddeCustomerSpouse;
	}

	public DDECustomer getDdeCustomer() {
		return ddeCustomer;
	}

	public void setDdeCustomer(DDECustomer ddeCustomer) {
		this.ddeCustomer = ddeCustomer;
	}

	public DDEKeyPerson getDdeKeyPerson() {
		return ddeKeyPerson;
	}

	public void setDdeKeyPerson(DDEKeyPerson ddeKeyPerson) {
		this.ddeKeyPerson = ddeKeyPerson;
	}

	public DDEUploadDocument getDdeUploadDocument() {
		return ddeUploadDocument;
	}

	public void setDdeUploadDocument(DDEUploadDocument ddeUploadDocument) {
		this.ddeUploadDocument = ddeUploadDocument;
	}
}
