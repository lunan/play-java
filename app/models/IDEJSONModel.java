package models;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import models.ide.IDEApplication;
import models.ide.IDECustomer;
import models.ide.IDECustomerFinanceSummary;
import models.ide.IDECustomerIncome;
import models.ide.IDECustomerSpouse;
import models.ide.IDECustomerWork;
import models.ide.IDELoanRequest;

public class IDEJSONModel extends JSONModel {
	@JsonUnwrapped(prefix="IDEApplication_")
	private IDEApplication ideApplication;
	
	@JsonUnwrapped(prefix="IDECustomer_")
	private IDECustomer ideCustomer;
	
	@JsonUnwrapped(prefix="IDECustomerSpouse_")
	private IDECustomerSpouse ideCustomerSpouse;
	
	@JsonUnwrapped(prefix="IDELoanRequest_")
	private IDELoanRequest ideLoanRequest;
	
	@JsonUnwrapped(prefix="IDECustomerIncome_")
	private IDECustomerIncome ideCustomerIncome;
	
	@JsonUnwrapped(prefix="IDECustomerFinanceSummary_")
	private IDECustomerFinanceSummary ideCustomerFinanceSummary;
	
	@JsonUnwrapped(prefix="IDECustomerWork_")
	private IDECustomerWork ideCustomerWork;
	
	public IDEJSONModel() {
	}
	
	public IDEJSONModel(IDEApplication ideApplication) {
		super();
		this.ideApplication=ideApplication;
	}

	public IDEJSONModel(IDEApplication ideApplication, IDECustomer ideCustomer,
			IDECustomerSpouse ideCustomerSpouse, IDELoanRequest ideLoanRequest, IDECustomerIncome ideCustomerIncome,
			IDECustomerFinanceSummary ideCustomerFinanceSummary, IDECustomerWork ideCustomerWork) {
		super();
		this.ideApplication = ideApplication;
		this.ideCustomer = ideCustomer;
		this.ideCustomerSpouse = ideCustomerSpouse;
		this.ideLoanRequest = ideLoanRequest;
		this.ideCustomerIncome = ideCustomerIncome;
		this.ideCustomerFinanceSummary = ideCustomerFinanceSummary;
		this.ideCustomerWork = ideCustomerWork;
	}
	
	public void copyId(String id) {
		List<Field> fields = Arrays.asList(this.getClass().getDeclaredFields());
		for (Field field : fields) {
			if(!field.getName().equals("ideApplication")){
				field.setAccessible(true);
				Object fValue;
				try {
					fValue = field.get(this);
					Method method = fValue.getClass().getDeclaredMethod("setApplicationId", String.class);
					method.invoke(fValue, id);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<?> allFields(){
		List<Object> allField = new ArrayList<>();
		List<Field> fields = Arrays.asList(this.getClass().getDeclaredFields());
		for (Field field : fields) {
			field.setAccessible(true);			
			try {
				allField.add(field.get(this));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return allField;
	}

	public IDEApplication getIdeApplication() {
		return ideApplication;
	}

	public void setIdeApplication(IDEApplication ideApplication) {
		this.ideApplication = ideApplication;
	}

	public IDECustomer getIdeCustomer() {
		return ideCustomer;
	}

	public void setIdeCustomer(IDECustomer ideCustomer) {
		this.ideCustomer = ideCustomer;
	}

	public IDECustomerSpouse getIdeCustomerSpouse() {
		return ideCustomerSpouse;
	}

	public void setIdeCustomerSpouse(IDECustomerSpouse ideCustomerSpouse) {
		this.ideCustomerSpouse = ideCustomerSpouse;
	}

	public IDELoanRequest getIdeLoanRequest() {
		return ideLoanRequest;
	}

	public void setIdeLoanRequest(IDELoanRequest ideLoanRequest) {
		this.ideLoanRequest = ideLoanRequest;
	}

	public IDECustomerIncome getIdeCustomerIncome() {
		return ideCustomerIncome;
	}

	public void setIdeCustomerIncome(IDECustomerIncome ideCustomerIncome) {
		this.ideCustomerIncome = ideCustomerIncome;
	}

	public IDECustomerFinanceSummary getIdeCustomerFinanceSummary() {
		return ideCustomerFinanceSummary;
	}

	public void setIdeCustomerFinanceSummary(IDECustomerFinanceSummary ideCustomerFinanceSummary) {
		this.ideCustomerFinanceSummary = ideCustomerFinanceSummary;
	}

	public IDECustomerWork getIdeCustomerWork() {
		return ideCustomerWork;
	}

	public void setIdeCustomerWork(IDECustomerWork ideCustomerWork) {
		this.ideCustomerWork = ideCustomerWork;
	}

}
