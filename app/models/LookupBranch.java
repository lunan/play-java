package models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the LOOKUP_BRANCH database table.
 * 
 */
@Entity
@Table(name="LOOKUP_BRANCH", schema="EDW")
@NamedQuery(name="LookupBranch.findAll", query="SELECT l FROM LookupBranch l")
public class LookupBranch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BRANCH_CD")
	private String branchCd;

	private String area;

	@Column(name="BRANCH_ADDR")
	private String branchAddr;

	@Column(name="BRANCH_CD_BI")
	private String branchCdBi;

	@Column(name="BRANCH_CD_MIS")
	private String branchCdMis;

	@Column(name="BRANCH_INDUK")
	private String branchInduk;

	@Column(name="BRANCH_NM")
	private String branchNm;

	@Column(name="BRANCH_TYPE")
	private String branchType;

	@Column(name="DATI2_CD")
	private String dati2Cd;

	private String fax;

	@Column(name="FOG_OFFICE")
	private String fogOffice;

	private String jabatan;

	private String kabupaten;

	@Column(name="KABUPATEN_BI")
	private String kabupatenBi;

	private String kodepos;

	private String kota;

	private String mnemonic;

	private String pinca;

	@Column(name="PINCA_TELP")
	private String pincaTelp;

	private String propinsi;

	@Column(name="PROPINSI_BI")
	private String propinsiBi;

	private String provinsi;

	@Column(name="REGION_CD")
	private String regionCd;

	private String telpon;

	public LookupBranch() {
	}

	public String getBranchCd() {
		return this.branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getBranchAddr() {
		return this.branchAddr;
	}

	public void setBranchAddr(String branchAddr) {
		this.branchAddr = branchAddr;
	}

	public String getBranchCdBi() {
		return this.branchCdBi;
	}

	public void setBranchCdBi(String branchCdBi) {
		this.branchCdBi = branchCdBi;
	}

	public String getBranchCdMis() {
		return this.branchCdMis;
	}

	public void setBranchCdMis(String branchCdMis) {
		this.branchCdMis = branchCdMis;
	}

	public String getBranchInduk() {
		return this.branchInduk;
	}

	public void setBranchInduk(String branchInduk) {
		this.branchInduk = branchInduk;
	}

	public String getBranchNm() {
		return this.branchNm;
	}

	public void setBranchNm(String branchNm) {
		this.branchNm = branchNm;
	}

	public String getBranchType() {
		return this.branchType;
	}

	public void setBranchType(String branchType) {
		this.branchType = branchType;
	}

	public String getDati2Cd() {
		return this.dati2Cd;
	}

	public void setDati2Cd(String dati2Cd) {
		this.dati2Cd = dati2Cd;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFogOffice() {
		return this.fogOffice;
	}

	public void setFogOffice(String fogOffice) {
		this.fogOffice = fogOffice;
	}

	public String getJabatan() {
		return this.jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}

	public String getKabupaten() {
		return this.kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getKabupatenBi() {
		return this.kabupatenBi;
	}

	public void setKabupatenBi(String kabupatenBi) {
		this.kabupatenBi = kabupatenBi;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getKota() {
		return this.kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getMnemonic() {
		return this.mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getPinca() {
		return this.pinca;
	}

	public void setPinca(String pinca) {
		this.pinca = pinca;
	}

	public String getPincaTelp() {
		return this.pincaTelp;
	}

	public void setPincaTelp(String pincaTelp) {
		this.pincaTelp = pincaTelp;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getPropinsiBi() {
		return this.propinsiBi;
	}

	public void setPropinsiBi(String propinsiBi) {
		this.propinsiBi = propinsiBi;
	}

	public String getProvinsi() {
		return this.provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getRegionCd() {
		return this.regionCd;
	}

	public void setRegionCd(String regionCd) {
		this.regionCd = regionCd;
	}

	public String getTelpon() {
		return this.telpon;
	}

	public void setTelpon(String telpon) {
		this.telpon = telpon;
	}

}