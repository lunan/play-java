package models.utl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


/**
 * The persistent class for the UTL_KODEPOS database table.
 * 
 */
@Entity
@Table(name="UTL_KODEPOS")
@SqlResultSetMapping(name="valMapping", columns={@ColumnResult(name="VAL")} )
@NamedQuery(name="BSMAddress.findAll", query="SELECT u FROM BSMAddress u")

public class BSMAddress implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String GET_PROPINSI = "SELECT DISTINCT PROPINSI FROM WISE.UTL_KODEPOS ORDER BY PROPINSI";
	public static final String GET_KAB_KOTA = "SELECT DISTINCT KABUPATEN_KOTA FROM WISE.UTL_KODEPOS WHERE PROPINSI= :propinsi";
	public static final String GET_KECAMATAN = "SELECT DISTINCT KECAMATAN FROM WISE.UTL_KODEPOS WHERE PROPINSI = :propinsi AND KABUPATEN_KOTA= :kabupatenKota";
	public static final String GET_KELURAHAN = "SELECT KELURAHAN,KODEPOS FROM WISE.UTL_KODEPOS WHERE PROPINSI = :propinsi AND KABUPATEN_KOTA= :kabupatenKota AND KECAMATAN= :kecamatan ORDER BY KELURAHAN";
	
	@Id
	@Column(name="\"ID\"")
	private int id;

	private String jenis;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	private String propinsi;

	public BSMAddress() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJenis() {
		return this.jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

}