package models.dde;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ORIG_CUSTOMER_EMERGENCY database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_EMERGENCY", schema="WISE")
@NamedQuery(name="DDECustomerEmergency.findAll", query="SELECT d FROM DDECustomerEmergency d")
public class DDECustomerEmergency implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="\"ALIAS\"")
	private String alias;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="FULL_NAME")
	private String fullName;

	private String hp1;

	private String hp2;

	private String kabupaten;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LEGAL_NAME")
	private String legalName;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	private String phone;

	private String propinsi;

	private String relation;

	private int rt;

	private int rw;

	public DDECustomerEmergency() {
	}

	public DDECustomerEmergency(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getHp1() {
		return this.hp1;
	}

	public void setHp1(String hp1) {
		this.hp1 = hp1;
	}

	public String getHp2() {
		return this.hp2;
	}

	public void setHp2(String hp2) {
		this.hp2 = hp2;
	}

	public String getKabupaten() {
		return this.kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getRelation() {
		return this.relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

}