package models.dde;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_LOAN_REQUEST database table.
 * 
 */
@Entity
@Table(name="ORIG_LOAN_REQUEST", schema="WISE")
@NamedQuery(name="DDELoanRequest.findAll", query="SELECT d FROM DDELoanRequest d")
public class DDELoanRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="BANK_PORTION")
	private String bankPortion;

	@Column(name="BANK_RATIO")
	private String bankRatio;

	@Column(name="CAPITAL_CUSTOMER")
	private String capitalCustomer;

	private String category;

	@Column(name="COLL_TO_LOAN_RATIO")
	private String collToLoanRatio;

	@Column(name="COLLATERAL_TYPE")
	private BigDecimal collateralType;

	@Column(name="CUSTOMER_PORTION")
	private String customerPortion;

	@Column(name="CUSTOMER_RATIO")
	private String customerRatio;

	@Column(name="CUSTOMER_TURNOVER")
	private String customerTurnover;

	@Column(name="DOWN_PAYMENT")
	private BigDecimal downPayment;

	@Column(name="DUE_DATE")
	private String dueDate;

	@Column(name="FINANCING_SCHEMA")
	private String financingSchema;

	@Column(name="FINANCING_TYPE")
	private String financingType;

	@Column(name="FINANCING_VALUE")
	private String financingValue;

	@Column(name="INSTALLMENT_TYPE")
	private String installmentType;

	@Column(name="INSTALLMENT_VALUE")
	private BigDecimal installmentValue;

	@Column(name="IS_PREVIOUSLY_FINANCING")
	private String isPreviouslyFinancing;

	@Column(name="LOAN_PURPOSE")
	private String loanPurpose;

	@Column(name="LOAN_TERM")
	private int loanTerm;

	@Column(name="LOAN_TO_COLL_RATIO")
	private BigDecimal loanToCollRatio;

	@Column(name="MARGIN_RATE")
	private String marginRate;

	@Column(name="MARGIN_TYPE")
	private String marginType;

	@Column(name="MARGIN_VALUE")
	private String marginValue;

	@Column(name="PRODUCT_TYPE")
	private String productType;

	@Column(name="PROFIT_SHARING")
	private String profitSharing;

	private String qard;

	@Column(name="SALES_PROJECTION")
	private String salesProjection;

	@Column(name="SELLING_PRICE")
	private String sellingPrice;

	@Column(name="SETTLEMENT_SOURCE")
	private String settlementSource;

	@Column(name="SETTLEMENT_SYSTEM")
	private String settlementSystem;

	private String tenor;

	@Column(name="TOTAL_CAPITAL")
	private String totalCapital;

	@Column(name="TOTAL_COLL_MARKET_VALUE")
	private BigDecimal totalCollMarketValue;

	public DDELoanRequest() {
	}

	public DDELoanRequest(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getBankPortion() {
		return this.bankPortion;
	}

	public void setBankPortion(String bankPortion) {
		this.bankPortion = bankPortion;
	}

	public String getBankRatio() {
		return this.bankRatio;
	}

	public void setBankRatio(String bankRatio) {
		this.bankRatio = bankRatio;
	}

	public String getCapitalCustomer() {
		return this.capitalCustomer;
	}

	public void setCapitalCustomer(String capitalCustomer) {
		this.capitalCustomer = capitalCustomer;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCollToLoanRatio() {
		return this.collToLoanRatio;
	}

	public void setCollToLoanRatio(String collToLoanRatio) {
		this.collToLoanRatio = collToLoanRatio;
	}

	public BigDecimal getCollateralType() {
		return this.collateralType;
	}

	public void setCollateralType(BigDecimal collateralType) {
		this.collateralType = collateralType;
	}

	public String getCustomerPortion() {
		return this.customerPortion;
	}

	public void setCustomerPortion(String customerPortion) {
		this.customerPortion = customerPortion;
	}

	public String getCustomerRatio() {
		return this.customerRatio;
	}

	public void setCustomerRatio(String customerRatio) {
		this.customerRatio = customerRatio;
	}

	public String getCustomerTurnover() {
		return this.customerTurnover;
	}

	public void setCustomerTurnover(String customerTurnover) {
		this.customerTurnover = customerTurnover;
	}

	public BigDecimal getDownPayment() {
		return this.downPayment;
	}

	public void setDownPayment(BigDecimal downPayment) {
		this.downPayment = downPayment;
	}

	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getFinancingSchema() {
		return this.financingSchema;
	}

	public void setFinancingSchema(String financingSchema) {
		this.financingSchema = financingSchema;
	}

	public String getFinancingType() {
		return this.financingType;
	}

	public void setFinancingType(String financingType) {
		this.financingType = financingType;
	}

	public String getFinancingValue() {
		return this.financingValue;
	}

	public void setFinancingValue(String financingValue) {
		this.financingValue = financingValue;
	}

	public String getInstallmentType() {
		return this.installmentType;
	}

	public void setInstallmentType(String installmentType) {
		this.installmentType = installmentType;
	}

	public BigDecimal getInstallmentValue() {
		return this.installmentValue;
	}

	public void setInstallmentValue(BigDecimal installmentValue) {
		this.installmentValue = installmentValue;
	}

	public String getIsPreviouslyFinancing() {
		return this.isPreviouslyFinancing;
	}

	public void setIsPreviouslyFinancing(String isPreviouslyFinancing) {
		this.isPreviouslyFinancing = isPreviouslyFinancing;
	}

	public String getLoanPurpose() {
		return this.loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public int getLoanTerm() {
		return this.loanTerm;
	}

	public void setLoanTerm(int loanTerm) {
		this.loanTerm = loanTerm;
	}

	public BigDecimal getLoanToCollRatio() {
		return this.loanToCollRatio;
	}

	public void setLoanToCollRatio(BigDecimal loanToCollRatio) {
		this.loanToCollRatio = loanToCollRatio;
	}

	public String getMarginRate() {
		return this.marginRate;
	}

	public void setMarginRate(String marginRate) {
		this.marginRate = marginRate;
	}

	public String getMarginType() {
		return this.marginType;
	}

	public void setMarginType(String marginType) {
		this.marginType = marginType;
	}

	public String getMarginValue() {
		return this.marginValue;
	}

	public void setMarginValue(String marginValue) {
		this.marginValue = marginValue;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProfitSharing() {
		return this.profitSharing;
	}

	public void setProfitSharing(String profitSharing) {
		this.profitSharing = profitSharing;
	}

	public String getQard() {
		return this.qard;
	}

	public void setQard(String qard) {
		this.qard = qard;
	}

	public String getSalesProjection() {
		return this.salesProjection;
	}

	public void setSalesProjection(String salesProjection) {
		this.salesProjection = salesProjection;
	}

	public String getSellingPrice() {
		return this.sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getSettlementSource() {
		return this.settlementSource;
	}

	public void setSettlementSource(String settlementSource) {
		this.settlementSource = settlementSource;
	}

	public String getSettlementSystem() {
		return this.settlementSystem;
	}

	public void setSettlementSystem(String settlementSystem) {
		this.settlementSystem = settlementSystem;
	}

	public String getTenor() {
		return this.tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTotalCapital() {
		return this.totalCapital;
	}

	public void setTotalCapital(String totalCapital) {
		this.totalCapital = totalCapital;
	}

	public BigDecimal getTotalCollMarketValue() {
		return this.totalCollMarketValue;
	}

	public void setTotalCollMarketValue(BigDecimal totalCollMarketValue) {
		this.totalCollMarketValue = totalCollMarketValue;
	}

}