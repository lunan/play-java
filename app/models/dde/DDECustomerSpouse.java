package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUSTOMER_SPOUSE database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_SPOUSE", schema="WISE")
@NamedQuery(name="DDECustomerSpouse.findAll", query="SELECT d FROM DDECustomerSpouse d")
public class DDECustomerSpouse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="\"ALIAS\"")
	private String alias;

	private Timestamp dob;

	private String email;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="FULL_NAME")
	private String fullName;

	private String hp1;

	private String hp2;

	private String kabupaten;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	@Column(name="LEGAL_ID_NO")
	private String legalIdNo;

	@Column(name="LEGAL_KABUPATEN")
	private String legalKabupaten;

	@Column(name="LEGAL_KECAMATAN")
	private String legalKecamatan;

	@Column(name="LEGAL_KELURAHAN")
	private String legalKelurahan;

	@Column(name="LEGAL_KODEPOS")
	private String legalKodepos;

	@Column(name="LEGAL_NAME")
	private String legalName;

	@Column(name="LEGAL_PROPINSI")
	private String legalPropinsi;

	@Column(name="LEGAL_RT")
	private int legalRt;

	@Column(name="LEGAL_RW")
	private int legalRw;

	@Column(name="LEGAL_TYPE")
	private String legalType;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	@Column(name="MOTHER_MAIDEN_NAME")
	private String motherMaidenName;

	private String phone;

	private String pob;

	private String propinsi;

	private String religion;

	private int rt;

	private int rw;

	public DDECustomerSpouse() {
	}

	public DDECustomerSpouse(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getDob() {
		return TimestampTransformer.timestampToString(this.dob);
	}

	public void setDob(String dob) {
		this.dob = TimestampTransformer.stringToTimestamp(dob);
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getHp1() {
		return this.hp1;
	}

	public void setHp1(String hp1) {
		this.hp1 = hp1;
	}

	public String getHp2() {
		return this.hp2;
	}

	public void setHp2(String hp2) {
		this.hp2 = hp2;
	}

	public String getKabupaten() {
		return this.kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalAddress() {
		return this.legalAddress;
	}

	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	public String getLegalKabupaten() {
		return this.legalKabupaten;
	}

	public void setLegalKabupaten(String legalKabupaten) {
		this.legalKabupaten = legalKabupaten;
	}

	public String getLegalKecamatan() {
		return this.legalKecamatan;
	}

	public void setLegalKecamatan(String legalKecamatan) {
		this.legalKecamatan = legalKecamatan;
	}

	public String getLegalKelurahan() {
		return this.legalKelurahan;
	}

	public void setLegalKelurahan(String legalKelurahan) {
		this.legalKelurahan = legalKelurahan;
	}

	public String getLegalKodepos() {
		return this.legalKodepos;
	}

	public void setLegalKodepos(String legalKodepos) {
		this.legalKodepos = legalKodepos;
	}

	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalPropinsi() {
		return this.legalPropinsi;
	}

	public void setLegalPropinsi(String legalPropinsi) {
		this.legalPropinsi = legalPropinsi;
	}

	public int getLegalRt() {
		return this.legalRt;
	}

	public void setLegalRt(int legalRt) {
		this.legalRt = legalRt;
	}

	public int getLegalRw() {
		return this.legalRw;
	}

	public void setLegalRw(int legalRw) {
		this.legalRw = legalRw;
	}

	public String getLegalType() {
		return this.legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMotherMaidenName() {
		return this.motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPob() {
		return this.pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

}