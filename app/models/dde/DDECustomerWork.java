package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUSTOMER_WORK database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_WORK", schema="WISE")
@NamedQuery(name="DDECustomerWork.findAll", query="SELECT d FROM DDECustomerWork d")
public class DDECustomerWork implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="ADDL_ADDRESS")
	private String addlAddress;

	@Column(name="ADDL_BUSINESS_FIELD")
	private String addlBusinessField;

	@Column(name="ADDL_CORP_NAME")
	private String addlCorpName;

	@Column(name="ADDL_EXT")
	private String addlExt;

	@Column(name="ADDL_FAX")
	private String addlFax;

	@Column(name="ADDL_KABUPATEN_KOTA")
	private String addlKabupatenKota;

	@Column(name="ADDL_KECAMATAN")
	private String addlKecamatan;

	@Column(name="ADDL_KELURAHAN")
	private String addlKelurahan;

	@Column(name="ADDL_KODEPOS")
	private String addlKodepos;

	@Column(name="ADDL_LEGAL_ENTITY")
	private String addlLegalEntity;

	@Column(name="ADDL_NUMBER_OF_EMPLOYEE")
	private int addlNumberOfEmployee;

	@Column(name="ADDL_PHONE")
	private String addlPhone;

	@Column(name="ADDL_POSITION")
	private String addlPosition;

	@Column(name="ADDL_PROPINSI")
	private String addlPropinsi;

	@Column(name="ADDL_RT")
	private int addlRt;

	@Column(name="ADDL_RW")
	private String addlRw;

	@Column(name="ADDL_SUPERVISOR")
	private String addlSupervisor;

	@Column(name="ADDL_SUPERVISOR_PHONE")
	private String addlSupervisorPhone;

	@Column(name="ADDL_TYPE_OF_WORK")
	private String addlTypeOfWork;

	@Column(name="ADDL_WORK")
	private String addlWork;

	@Column(name="ADDL_WORKING_SINCE")
	private Timestamp addlWorkingSince;

	@Column(name="ADDL_WORKPLACE")
	private String addlWorkplace;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="BUSINESS_FIELD")
	private String businessField;

	@Column(name="CORP_NAME")
	private String corpName;

	@Column(name="ECO_SECTOR")
	private String ecoSector;

	private String ext;

	private String fax;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LEGAL_ENTITY")
	private String legalEntity;

	@Column(name="NUMBER_OF_EMPLOYEE")
	private int numberOfEmployee;

	private String phone;

	@Column(name="\"POSITION\"")
	private String position;

	private String propinsi;

	private int rt;

	private int rw;

	private String supervisor;

	@Column(name="SUPERVISOR_PHONE")
	private String supervisorPhone;

	@Column(name="TYPE_OF_WORK")
	private String typeOfWork;

	@Column(name="\"WORK\"")
	private String work;

	@Column(name="WORK_EXP_OTHER")
	private String workExpOther;

	@Column(name="WORKING_SINCE")
	private Timestamp workingSince;

	private String workplace;

	public DDECustomerWork() {
	}

	public DDECustomerWork(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddlAddress() {
		return this.addlAddress;
	}

	public void setAddlAddress(String addlAddress) {
		this.addlAddress = addlAddress;
	}

	public String getAddlBusinessField() {
		return this.addlBusinessField;
	}

	public void setAddlBusinessField(String addlBusinessField) {
		this.addlBusinessField = addlBusinessField;
	}

	public String getAddlCorpName() {
		return this.addlCorpName;
	}

	public void setAddlCorpName(String addlCorpName) {
		this.addlCorpName = addlCorpName;
	}

	public String getAddlExt() {
		return this.addlExt;
	}

	public void setAddlExt(String addlExt) {
		this.addlExt = addlExt;
	}

	public String getAddlFax() {
		return this.addlFax;
	}

	public void setAddlFax(String addlFax) {
		this.addlFax = addlFax;
	}

	public String getAddlKabupatenKota() {
		return this.addlKabupatenKota;
	}

	public void setAddlKabupatenKota(String addlKabupatenKota) {
		this.addlKabupatenKota = addlKabupatenKota;
	}

	public String getAddlKecamatan() {
		return this.addlKecamatan;
	}

	public void setAddlKecamatan(String addlKecamatan) {
		this.addlKecamatan = addlKecamatan;
	}

	public String getAddlKelurahan() {
		return this.addlKelurahan;
	}

	public void setAddlKelurahan(String addlKelurahan) {
		this.addlKelurahan = addlKelurahan;
	}

	public String getAddlKodepos() {
		return this.addlKodepos;
	}

	public void setAddlKodepos(String addlKodepos) {
		this.addlKodepos = addlKodepos;
	}

	public String getAddlLegalEntity() {
		return this.addlLegalEntity;
	}

	public void setAddlLegalEntity(String addlLegalEntity) {
		this.addlLegalEntity = addlLegalEntity;
	}

	public int getAddlNumberOfEmployee() {
		return this.addlNumberOfEmployee;
	}

	public void setAddlNumberOfEmployee(int addlNumberOfEmployee) {
		this.addlNumberOfEmployee = addlNumberOfEmployee;
	}

	public String getAddlPhone() {
		return this.addlPhone;
	}

	public void setAddlPhone(String addlPhone) {
		this.addlPhone = addlPhone;
	}

	public String getAddlPosition() {
		return this.addlPosition;
	}

	public void setAddlPosition(String addlPosition) {
		this.addlPosition = addlPosition;
	}

	public String getAddlPropinsi() {
		return this.addlPropinsi;
	}

	public void setAddlPropinsi(String addlPropinsi) {
		this.addlPropinsi = addlPropinsi;
	}

	public int getAddlRt() {
		return this.addlRt;
	}

	public void setAddlRt(int addlRt) {
		this.addlRt = addlRt;
	}

	public String getAddlRw() {
		return this.addlRw;
	}

	public void setAddlRw(String addlRw) {
		this.addlRw = addlRw;
	}

	public String getAddlSupervisor() {
		return this.addlSupervisor;
	}

	public void setAddlSupervisor(String addlSupervisor) {
		this.addlSupervisor = addlSupervisor;
	}

	public String getAddlSupervisorPhone() {
		return this.addlSupervisorPhone;
	}

	public void setAddlSupervisorPhone(String addlSupervisorPhone) {
		this.addlSupervisorPhone = addlSupervisorPhone;
	}

	public String getAddlTypeOfWork() {
		return this.addlTypeOfWork;
	}

	public void setAddlTypeOfWork(String addlTypeOfWork) {
		this.addlTypeOfWork = addlTypeOfWork;
	}

	public String getAddlWork() {
		return this.addlWork;
	}

	public void setAddlWork(String addlWork) {
		this.addlWork = addlWork;
	}

	public String getAddlWorkingSince() {
		return TimestampTransformer.timestampToString(this.addlWorkingSince);
	}

	public void setAddlWorkingSince(String addlWorkingSince) {
		this.addlWorkingSince = TimestampTransformer.stringToTimestamp(addlWorkingSince);
	}

	public String getAddlWorkplace() {
		return this.addlWorkplace;
	}

	public void setAddlWorkplace(String addlWorkplace) {
		this.addlWorkplace = addlWorkplace;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusinessField() {
		return this.businessField;
	}

	public void setBusinessField(String businessField) {
		this.businessField = businessField;
	}

	public String getCorpName() {
		return this.corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getEcoSector() {
		return this.ecoSector;
	}

	public void setEcoSector(String ecoSector) {
		this.ecoSector = ecoSector;
	}

	public String getExt() {
		return this.ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLegalEntity() {
		return this.legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	public int getNumberOfEmployee() {
		return this.numberOfEmployee;
	}

	public void setNumberOfEmployee(int numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

	public String getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getSupervisorPhone() {
		return this.supervisorPhone;
	}

	public void setSupervisorPhone(String supervisorPhone) {
		this.supervisorPhone = supervisorPhone;
	}

	public String getTypeOfWork() {
		return this.typeOfWork;
	}

	public void setTypeOfWork(String typeOfWork) {
		this.typeOfWork = typeOfWork;
	}

	public String getWork() {
		return this.work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public String getWorkExpOther() {
		return this.workExpOther;
	}

	public void setWorkExpOther(String workExpOther) {
		this.workExpOther = workExpOther;
	}

	public String getWorkingSince() {
		return TimestampTransformer.timestampToString(this.workingSince);
	}

	public void setWorkingSince(String workingSince) {
		this.workingSince = TimestampTransformer.stringToTimestamp(workingSince);
	}

	public String getWorkplace() {
		return this.workplace;
	}

	public void setWorkplace(String workplace) {
		this.workplace = workplace;
	}

}