package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUSTOMER_SPOUSE_WORK database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_SPOUSE_WORK", schema="WISE")
@NamedQuery(name="DDECustomerSpouseWork.findAll", query="SELECT d FROM DDECustomerSpouseWork d")
public class DDECustomerSpouseWork implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="BUSINESS_FIELD")
	private String businessField;

	@Column(name="CORP_NAME")
	private String corpName;

	@Column(name="ECONOMIC_SECTOR")
	private String economicSector;

	private String ext;

	private String fax;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LEGAL_ENTITY")
	private String legalEntity;

	@Column(name="NUMBER_OF_EMPLOYEE")
	private int numberOfEmployee;

	private String phone;

	@Column(name="\"POSITION\"")
	private String position;

	private String propinsi;

	private int rt;

	private int rw;

	private String supervisor;

	@Column(name="SUPERVISOR_PHONE")
	private String supervisorPhone;

	@Column(name="TYPE_OF_WORK")
	private String typeOfWork;

	@Column(name="WORK_EXP_OTHER")
	private String workExpOther;

	@Column(name="WORKING_SINCE")
	private Timestamp workingSince;

	public DDECustomerSpouseWork() {
	}

	public DDECustomerSpouseWork(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusinessField() {
		return this.businessField;
	}

	public void setBusinessField(String businessField) {
		this.businessField = businessField;
	}

	public String getCorpName() {
		return this.corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getEconomicSector() {
		return this.economicSector;
	}

	public void setEconomicSector(String economicSector) {
		this.economicSector = economicSector;
	}

	public String getExt() {
		return this.ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLegalEntity() {
		return this.legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	public int getNumberOfEmployee() {
		return this.numberOfEmployee;
	}

	public void setNumberOfEmployee(int numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

	public String getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getSupervisorPhone() {
		return this.supervisorPhone;
	}

	public void setSupervisorPhone(String supervisorPhone) {
		this.supervisorPhone = supervisorPhone;
	}

	public String getTypeOfWork() {
		return this.typeOfWork;
	}

	public void setTypeOfWork(String typeOfWork) {
		this.typeOfWork = typeOfWork;
	}

	public String getWorkExpOther() {
		return this.workExpOther;
	}

	public void setWorkExpOther(String workExpOther) {
		this.workExpOther = workExpOther;
	}

	public String getWorkingSince() {
		return TimestampTransformer.timestampToString(this.workingSince);
	}

	public void setWorkingSince(String workingSince) {
		this.workingSince = TimestampTransformer.stringToTimestamp(workingSince);
	}

}