package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_FINANCE_CC database table.
 * 
 */
@Entity
@Table(name="ORIG_FINANCE_CC", schema="WISE")
@NamedQuery(name="DDEFinanceCc.findAll", query="SELECT d FROM DDEFinanceCc d")
public class DDEFinanceCc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	private BigDecimal arrears;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="CARD_NO")
	private String cardNo;

	@Column(name="\"LIMIT\"")
	private BigDecimal limit;

	private BigDecimal outstanding;

	private Timestamp since;

	public DDEFinanceCc() {
	}

	public DDEFinanceCc(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getArrears() {
		return this.arrears;
	}

	public void setArrears(BigDecimal arrears) {
		this.arrears = arrears;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCardNo() {
		return this.cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public BigDecimal getLimit() {
		return this.limit;
	}

	public void setLimit(BigDecimal limit) {
		this.limit = limit;
	}

	public BigDecimal getOutstanding() {
		return this.outstanding;
	}

	public void setOutstanding(BigDecimal outstanding) {
		this.outstanding = outstanding;
	}

	public String getSince() {
		return TimestampTransformer.timestampToString(this.since);
	}

	public void setSince(String since) {
		this.since = TimestampTransformer.stringToTimestamp(since);
	}

}