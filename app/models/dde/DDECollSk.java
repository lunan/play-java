package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_SK database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_SK", schema="WISE")
@NamedQuery(name="DDECollSk.findAll", query="SELECT d FROM DDECollSk d")
public class DDECollSk implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	private String institution;

	@Column(name="IS_BSM_PAYROLL")
	private String isBsmPayroll;

	@Column(name="PKS_NO")
	private String pksNo;

	@Column(name="SK_DT")
	private Timestamp skDt;

	@Column(name="SK_NO")
	private String skNo;

	@Column(name="TREASURER_ACCT")
	private String treasurerAcct;

	@Column(name="TREASURER_NM")
	private String treasurerNm;

	public DDECollSk() {
	}

	public DDECollSk(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getInstitution() {
		return this.institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getIsBsmPayroll() {
		return this.isBsmPayroll;
	}

	public void setIsBsmPayroll(String isBsmPayroll) {
		this.isBsmPayroll = isBsmPayroll;
	}

	public String getPksNo() {
		return this.pksNo;
	}

	public void setPksNo(String pksNo) {
		this.pksNo = pksNo;
	}

	public String getSkDt() {
		return TimestampTransformer.timestampToString(this.skDt);
	}

	public void setSkDt(String skDt) {
		this.skDt = TimestampTransformer.stringToTimestamp(skDt);
	}

	public String getSkNo() {
		return this.skNo;
	}

	public void setSkNo(String skNo) {
		this.skNo = skNo;
	}

	public String getTreasurerAcct() {
		return this.treasurerAcct;
	}

	public void setTreasurerAcct(String treasurerAcct) {
		this.treasurerAcct = treasurerAcct;
	}

	public String getTreasurerNm() {
		return this.treasurerNm;
	}

	public void setTreasurerNm(String treasurerNm) {
		this.treasurerNm = treasurerNm;
	}

}