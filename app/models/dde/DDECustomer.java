package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUSTOMER database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER", schema="WISE")
@NamedQuery(name="DDECustomer.findAll", query="SELECT d FROM DDECustomer d")
public class DDECustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="\"ALIAS\"")
	private String alias;

	@Column(name="ASSET_IMPROVEMENT")
	private String assetImprovement;

	@Column(name="BILLING_ADDRESS")
	private String billingAddress;

	private String bookkeeping;

	@Column(name="BUSINESS_CONDITION")
	private String businessCondition;

	@Column(name="BUSINESS_GROUP_INFROMATION")
	private String businessGroupInfromation;

	@Column(name="BUSINESS_LOCATION")
	private String businessLocation;

	@Column(name="COMPANY_LEGAL_NAME")
	private String companyLegalName;

	@Column(name="CORP_ADDRESS")
	private String corpAddress;

	@Column(name="CORP_HQ_OWNERSHIP")
	private String corpHqOwnership;

	@Column(name="CORP_LEGAL_TYPE")
	private String corpLegalType;

	@Column(name="CORP_NAME")
	private String corpName;

	@Column(name="CORP_PHONE")
	private String corpPhone;

	@Column(name="CORP_SINCE")
	private String corpSince;

	@Column(name="CUSTOMER_BASE")
	private String customerBase;

	@Column(name="DEBT_CAPACITY")
	private String debtCapacity;

	@Column(name="DEBTOR_RELATIONSHIP")
	private String debtorRelationship;

	@Column(name="\"DEPENDENT\"")
	private int dependent=0;

	private Timestamp dob;

	@Column(name="EASE_OF_RAW_MATERIAL")
	private String easeOfRawMaterial;

	@Column(name="ECONOMIC_SECTOR")
	private String economicSector;

	private String education;

	private String email;

	@Column(name="END_USER")
	private String endUser;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="FULL_NAME")
	private String fullName;

	private String gender;

	@Column(name="GOLONGAN_DEBITUR")
	private String golonganDebitur;

	@Column(name="GOVERNMENT_EFFECT")
	private String governmentEffect;

	@Column(name="HOUSE_STATUS")
	private String houseStatus;

	private String hp1;

	private String hp2;

	private String hp3;

	private String infrastructure;

	@Column(name="IS_HOUSE_PLEDGED")
	private String isHousePledged;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	@Column(name="LEGAL_ID_NO", nullable=false)
	private String legalIdNo;

	@Column(name="LEGAL_KABUPATEN_KOTA")
	private String legalKabupatenKota;

	@Column(name="LEGAL_KECAMATAN")
	private String legalKecamatan;

	@Column(name="LEGAL_KELURAHAN")
	private String legalKelurahan;

	@Column(name="LEGAL_KODEPOS")
	private String legalKodepos;

	@Column(name="LEGAL_NAME")
	private String legalName;

	@Column(name="LEGAL_PROPINSI")
	private String legalPropinsi;

	@Column(name="LEGAL_RT")
	private int legalRt;

	@Column(name="LEGAL_RW")
	private int legalRw;

	@Column(name="LEGAL_TYPE")
	private String legalType;

	@Column(name="LEVEL_OF_COMPETITION")
	private String levelOfCompetition;

	private String liquidity;

	@Column(name="MARITAL_STATUS")
	private String maritalStatus;

	@Column(name="MARKET_CONDITIONS")
	private String marketConditions;

	@Column(name="MARKETING_MANAGEMENT")
	private String marketingManagement;

	@Column(name="MARKETING_PROSPECT")
	private String marketingProspect;

	@Column(name="MARKETING_TARGET")
	private String marketingTarget;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	@Column(name="MONTH_OF_RESIDENCY")
	private int monthOfResidency;

	@Column(name="MOTHER_MAIDEN_NAME")
	private String motherMaidenName;

	@Column(name="NUM_OF_EMPLOYEE")
	private int numOfEmployee;

	@Column(name="OPERATING_CAPACITY")
	private String operatingCapacity;

	private String phone;

	@Column(name="PKS_DATE")
	private Timestamp pksDate;

	@Column(name="PKS_NUMBER")
	private String pksNumber;

	@Column(name="PLEDGED_TO")
	private String pledgedTo;

	private String pob;

	private String propinsi;

	private String religion;

	private int rt;

	private int rw;

	@Column(name="SKKP_INDUK")
	private String skkpInduk;

	private String stewardship;

	@Column(name="TAX_ID")
	private String taxId;

	@Column(name="\"TYPE\"")
	private String type;

	@Column(name="TYPE_OF_BUSINESS")
	private String typeOfBusiness;

	@Column(name="WORK_UNIT")
	private String workUnit;

	public DDECustomer() {
	}

	public DDECustomer(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAssetImprovement() {
		return this.assetImprovement;
	}

	public void setAssetImprovement(String assetImprovement) {
		this.assetImprovement = assetImprovement;
	}

	public String getBillingAddress() {
		return this.billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBookkeeping() {
		return this.bookkeeping;
	}

	public void setBookkeeping(String bookkeeping) {
		this.bookkeeping = bookkeeping;
	}

	public String getBusinessCondition() {
		return this.businessCondition;
	}

	public void setBusinessCondition(String businessCondition) {
		this.businessCondition = businessCondition;
	}

	public String getBusinessGroupInfromation() {
		return this.businessGroupInfromation;
	}

	public void setBusinessGroupInfromation(String businessGroupInfromation) {
		this.businessGroupInfromation = businessGroupInfromation;
	}

	public String getBusinessLocation() {
		return this.businessLocation;
	}

	public void setBusinessLocation(String businessLocation) {
		this.businessLocation = businessLocation;
	}

	public String getCompanyLegalName() {
		return this.companyLegalName;
	}

	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}

	public String getCorpAddress() {
		return this.corpAddress;
	}

	public void setCorpAddress(String corpAddress) {
		this.corpAddress = corpAddress;
	}

	public String getCorpHqOwnership() {
		return this.corpHqOwnership;
	}

	public void setCorpHqOwnership(String corpHqOwnership) {
		this.corpHqOwnership = corpHqOwnership;
	}

	public String getCorpLegalType() {
		return this.corpLegalType;
	}

	public void setCorpLegalType(String corpLegalType) {
		this.corpLegalType = corpLegalType;
	}

	public String getCorpName() {
		return this.corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getCorpPhone() {
		return this.corpPhone;
	}

	public void setCorpPhone(String corpPhone) {
		this.corpPhone = corpPhone;
	}

	public String getCorpSince() {
		return this.corpSince;
	}

	public void setCorpSince(String corpSince) {
		this.corpSince = corpSince;
	}

	public String getCustomerBase() {
		return this.customerBase;
	}

	public void setCustomerBase(String customerBase) {
		this.customerBase = customerBase;
	}

	public String getDebtCapacity() {
		return this.debtCapacity;
	}

	public void setDebtCapacity(String debtCapacity) {
		this.debtCapacity = debtCapacity;
	}

	public String getDebtorRelationship() {
		return this.debtorRelationship;
	}

	public void setDebtorRelationship(String debtorRelationship) {
		this.debtorRelationship = debtorRelationship;
	}

	public int getDependent() {
		return this.dependent;
	}

	public void setDependent(int dependent) {
		this.dependent = dependent;
	}

	public String getDob() {
		return TimestampTransformer.timestampToString(this.dob);
	}

	public void setDob(String dob) {
		this.dob = TimestampTransformer.stringToTimestamp(dob);
	}

	public String getEaseOfRawMaterial() {
		return this.easeOfRawMaterial;
	}

	public void setEaseOfRawMaterial(String easeOfRawMaterial) {
		this.easeOfRawMaterial = easeOfRawMaterial;
	}

	public String getEconomicSector() {
		return this.economicSector;
	}

	public void setEconomicSector(String economicSector) {
		this.economicSector = economicSector;
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndUser() {
		return this.endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGolonganDebitur() {
		return this.golonganDebitur;
	}

	public void setGolonganDebitur(String golonganDebitur) {
		this.golonganDebitur = golonganDebitur;
	}

	public String getGovernmentEffect() {
		return this.governmentEffect;
	}

	public void setGovernmentEffect(String governmentEffect) {
		this.governmentEffect = governmentEffect;
	}

	public String getHouseStatus() {
		return this.houseStatus;
	}

	public void setHouseStatus(String houseStatus) {
		this.houseStatus = houseStatus;
	}

	public String getHp1() {
		return this.hp1;
	}

	public void setHp1(String hp1) {
		this.hp1 = hp1;
	}

	public String getHp2() {
		return this.hp2;
	}

	public void setHp2(String hp2) {
		this.hp2 = hp2;
	}

	public String getHp3() {
		return this.hp3;
	}

	public void setHp3(String hp3) {
		this.hp3 = hp3;
	}

	public String getInfrastructure() {
		return this.infrastructure;
	}

	public void setInfrastructure(String infrastructure) {
		this.infrastructure = infrastructure;
	}

	public String getIsHousePledged() {
		return this.isHousePledged;
	}

	public void setIsHousePledged(String isHousePledged) {
		this.isHousePledged = isHousePledged;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalAddress() {
		return this.legalAddress;
	}

	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	public String getLegalKabupatenKota() {
		return this.legalKabupatenKota;
	}

	public void setLegalKabupatenKota(String legalKabupatenKota) {
		this.legalKabupatenKota = legalKabupatenKota;
	}

	public String getLegalKecamatan() {
		return this.legalKecamatan;
	}

	public void setLegalKecamatan(String legalKecamatan) {
		this.legalKecamatan = legalKecamatan;
	}

	public String getLegalKelurahan() {
		return this.legalKelurahan;
	}

	public void setLegalKelurahan(String legalKelurahan) {
		this.legalKelurahan = legalKelurahan;
	}

	public String getLegalKodepos() {
		return this.legalKodepos;
	}

	public void setLegalKodepos(String legalKodepos) {
		this.legalKodepos = legalKodepos;
	}

	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalPropinsi() {
		return this.legalPropinsi;
	}

	public void setLegalPropinsi(String legalPropinsi) {
		this.legalPropinsi = legalPropinsi;
	}

	public int getLegalRt() {
		return this.legalRt;
	}

	public void setLegalRt(int legalRt) {
		this.legalRt = legalRt;
	}

	public int getLegalRw() {
		return this.legalRw;
	}

	public void setLegalRw(int legalRw) {
		this.legalRw = legalRw;
	}

	public String getLegalType() {
		return this.legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}

	public String getLevelOfCompetition() {
		return this.levelOfCompetition;
	}

	public void setLevelOfCompetition(String levelOfCompetition) {
		this.levelOfCompetition = levelOfCompetition;
	}

	public String getLiquidity() {
		return this.liquidity;
	}

	public void setLiquidity(String liquidity) {
		this.liquidity = liquidity;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMarketConditions() {
		return this.marketConditions;
	}

	public void setMarketConditions(String marketConditions) {
		this.marketConditions = marketConditions;
	}

	public String getMarketingManagement() {
		return this.marketingManagement;
	}

	public void setMarketingManagement(String marketingManagement) {
		this.marketingManagement = marketingManagement;
	}

	public String getMarketingProspect() {
		return this.marketingProspect;
	}

	public void setMarketingProspect(String marketingProspect) {
		this.marketingProspect = marketingProspect;
	}

	public String getMarketingTarget() {
		return this.marketingTarget;
	}

	public void setMarketingTarget(String marketingTarget) {
		this.marketingTarget = marketingTarget;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public int getMonthOfResidency() {
		return this.monthOfResidency;
	}

	public void setMonthOfResidency(int monthOfResidency) {
		this.monthOfResidency = monthOfResidency;
	}

	public String getMotherMaidenName() {
		return this.motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public int getNumOfEmployee() {
		return this.numOfEmployee;
	}

	public void setNumOfEmployee(int numOfEmployee) {
		this.numOfEmployee = numOfEmployee;
	}

	public String getOperatingCapacity() {
		return this.operatingCapacity;
	}

	public void setOperatingCapacity(String operatingCapacity) {
		this.operatingCapacity = operatingCapacity;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPksDate() {
		return TimestampTransformer.timestampToString(this.pksDate);
	}

	public void setPksDate(String pksDate) {
		this.pksDate = TimestampTransformer.stringToTimestamp(pksDate);
	}

	public String getPksNumber() {
		return this.pksNumber;
	}

	public void setPksNumber(String pksNumber) {
		this.pksNumber = pksNumber;
	}

	public String getPledgedTo() {
		return this.pledgedTo;
	}

	public void setPledgedTo(String pledgedTo) {
		this.pledgedTo = pledgedTo;
	}

	public String getPob() {
		return this.pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

	public String getSkkpInduk() {
		return this.skkpInduk;
	}

	public void setSkkpInduk(String skkpInduk) {
		this.skkpInduk = skkpInduk;
	}

	public String getStewardship() {
		return this.stewardship;
	}

	public void setStewardship(String stewardship) {
		this.stewardship = stewardship;
	}

	public String getTaxId() {
		return this.taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeOfBusiness() {
		return this.typeOfBusiness;
	}

	public void setTypeOfBusiness(String typeOfBusiness) {
		this.typeOfBusiness = typeOfBusiness;
	}

	public String getWorkUnit() {
		return this.workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}

}