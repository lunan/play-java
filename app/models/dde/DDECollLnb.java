package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_LNB database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_LNB", schema="WISE")
@NamedQuery(name="DDECollLnb.findAll", query="SELECT d FROM DDECollLnb d")
public class DDECollLnb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="BUILDING_AGE_YEARS")
	private int buildingAgeYears;

	@Column(name="BUILDING_AREA_M2")
	private BigDecimal buildingAreaM2;

	@Column(name="BUILDING_TYPE")
	private String buildingType;

	@Column(name="CERTIFICATE_DT")
	private Timestamp certificateDt;

	@Column(name="CERTIFICATE_DUE_DT")
	private Timestamp certificateDueDt;

	@Column(name="CERTIFICATE_NO")
	private String certificateNo;

	@Column(name="HOUSE_TYPE")
	private String houseType;

	private String imb;

	@Column(name="IS_CERT_EXIST")
	private String isCertExist;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LAND_AREA_M2")
	private BigDecimal landAreaM2;

	private String owner;

	@Column(name="\"OWNERSHIP\"")
	private String ownership;

	@Column(name="OWNERSHIP_STATUS")
	private String ownershipStatus;

	private String propinsi;

	private String relation;

	private int rt;

	private int rw;

	public DDECollLnb() {
	}

	public DDECollLnb(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getBuildingAgeYears() {
		return this.buildingAgeYears;
	}

	public void setBuildingAgeYears(int buildingAgeYears) {
		this.buildingAgeYears = buildingAgeYears;
	}

	public BigDecimal getBuildingAreaM2() {
		return this.buildingAreaM2;
	}

	public void setBuildingAreaM2(BigDecimal buildingAreaM2) {
		this.buildingAreaM2 = buildingAreaM2;
	}

	public String getBuildingType() {
		return this.buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	public String getCertificateDt() {
		return TimestampTransformer.timestampToString(this.certificateDt);
	}

	public void setCertificateDt(String certificateDt) {
		this.certificateDt = TimestampTransformer.stringToTimestamp(certificateDt);
	}

	public String getCertificateDueDt() {
		return TimestampTransformer.timestampToString(this.certificateDueDt);
	}

	public void setCertificateDueDt(String certificateDueDt) {
		this.certificateDueDt = TimestampTransformer.stringToTimestamp(certificateDueDt);
	}

	public String getCertificateNo() {
		return this.certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getHouseType() {
		return this.houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getImb() {
		return this.imb;
	}

	public void setImb(String imb) {
		this.imb = imb;
	}

	public String getIsCertExist() {
		return this.isCertExist;
	}

	public void setIsCertExist(String isCertExist) {
		this.isCertExist = isCertExist;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public BigDecimal getLandAreaM2() {
		return this.landAreaM2;
	}

	public void setLandAreaM2(BigDecimal landAreaM2) {
		this.landAreaM2 = landAreaM2;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnership() {
		return this.ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getOwnershipStatus() {
		return this.ownershipStatus;
	}

	public void setOwnershipStatus(String ownershipStatus) {
		this.ownershipStatus = ownershipStatus;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getRelation() {
		return this.relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

}