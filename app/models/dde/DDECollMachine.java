package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_MACHINE database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_MACHINE", schema="WISE")
@NamedQuery(name="DDECollMachine.findAll", query="SELECT d FROM DDECollMachine d")
public class DDECollMachine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"CONDITION\"")
	private String condition;

	@Column(name="FAKTUR_NO")
	private String fakturNo;

	private String manufacturer;

	@Column(name="MANUFACTURER_NATION")
	private String manufacturerNation;

	private String owner;

	@Column(name="OWNERSHIP_STATUS")
	private String ownershipStatus;

	@Column(name="PROOF_OF_OWNERSHIP")
	private String proofOfOwnership;

	@Column(name="\"TYPE\"")
	private String type;

	@Column(name="\"USAGE\"")
	private String usage;

	@Column(name="USAGE_STATUS")
	private String usageStatus;

	@Column(name="\"YEAR\"")
	private Timestamp year;

	public DDECollMachine() {
	}

	public DDECollMachine(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getFakturNo() {
		return this.fakturNo;
	}

	public void setFakturNo(String fakturNo) {
		this.fakturNo = fakturNo;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getManufacturerNation() {
		return this.manufacturerNation;
	}

	public void setManufacturerNation(String manufacturerNation) {
		this.manufacturerNation = manufacturerNation;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnershipStatus() {
		return this.ownershipStatus;
	}

	public void setOwnershipStatus(String ownershipStatus) {
		this.ownershipStatus = ownershipStatus;
	}

	public String getProofOfOwnership() {
		return this.proofOfOwnership;
	}

	public void setProofOfOwnership(String proofOfOwnership) {
		this.proofOfOwnership = proofOfOwnership;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsage() {
		return this.usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getUsageStatus() {
		return this.usageStatus;
	}

	public void setUsageStatus(String usageStatus) {
		this.usageStatus = usageStatus;
	}

	public String getYear() {
		return TimestampTransformer.timestampToString(this.year);
	}

	public void setYear(String year) {
		this.year = TimestampTransformer.stringToTimestamp(year);
	}

}