package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_LAND database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_LAND", schema="WISE")
@NamedQuery(name="DDECollLand.findAll", query="SELECT d FROM DDECollLand d")
public class DDECollLand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="AREA_M2")
	private BigDecimal areaM2;

	@Column(name="CERTIFICATE_DT")
	private Timestamp certificateDt;

	@Column(name="CERTIFICATE_DUE_DT")
	private Timestamp certificateDueDt;

	@Column(name="CERTIFICATE_NO")
	private String certificateNo;

	@Column(name="IS_CERT_EXIST")
	private String isCertExist;

	private String kabupaten;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LAND_CONDITION")
	private String landCondition;

	private String owner;

	@Column(name="\"OWNERSHIP\"")
	private String ownership;

	@Column(name="OWNERSHIP_STATUS")
	private String ownershipStatus;

	private String propinsi;

	private String relation;

	private int rt;

	private int rw;

	@Column(name="\"USAGE\"")
	private String usage;

	public DDECollLand() {
	}

	public DDECollLand(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getAreaM2() {
		return this.areaM2;
	}

	public void setAreaM2(BigDecimal areaM2) {
		this.areaM2 = areaM2;
	}

	public String getCertificateDt() {
		return TimestampTransformer.timestampToString(this.certificateDt);
	}

	public void setCertificateDt(String certificateDt) {
		this.certificateDt = TimestampTransformer.stringToTimestamp(certificateDt);
	}

	public String getCertificateDueDt() {
		return TimestampTransformer.timestampToString(this.certificateDueDt);
	}

	public void setCertificateDueDt(String certificateDueDt) {
		this.certificateDueDt = TimestampTransformer.stringToTimestamp(certificateDueDt);
	}

	public String getCertificateNo() {
		return this.certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getIsCertExist() {
		return this.isCertExist;
	}

	public void setIsCertExist(String isCertExist) {
		this.isCertExist = isCertExist;
	}

	public String getKabupaten() {
		return this.kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLandCondition() {
		return this.landCondition;
	}

	public void setLandCondition(String landCondition) {
		this.landCondition = landCondition;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnership() {
		return this.ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getOwnershipStatus() {
		return this.ownershipStatus;
	}

	public void setOwnershipStatus(String ownershipStatus) {
		this.ownershipStatus = ownershipStatus;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getRelation() {
		return this.relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

	public String getUsage() {
		return this.usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

}