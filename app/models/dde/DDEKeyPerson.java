package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_KEY_PERSON database table.
 * 
 */
@Entity
@Table(name="ORIG_KEY_PERSON", schema="WISE")
@NamedQuery(name="DDEKeyPerson.findAll", query="SELECT d FROM DDEKeyPerson d")
public class DDEKeyPerson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="\"CHARACTER\"")
	private String character;

	private Timestamp dob;

	private String education;

	private String email;

	@Column(name="FIRST_NAME")
	private String firstName;

	private String hp1;

	private String hp2;

	private String hp3;

	private String information;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	@Column(name="KEY_FLAG")
	private String keyFlag;

	private String kodepos;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LEGAL_ID_NO")
	private String legalIdNo;

	@Column(name="LEGAL_NAME")
	private String legalName;

	private String management;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	private String phone;

	private String pob;

	@Column(name="\"POSITION\"")
	private String position;

	private String propinsi;

	private String religion;

	private int rt;

	private int rw;

	public DDEKeyPerson() {
	}

	public DDEKeyPerson(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCharacter() {
		return this.character;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	public String getDob() {
		return TimestampTransformer.timestampToString(this.dob);
	}

	public void setDob(String dob) {
		this.dob = TimestampTransformer.stringToTimestamp(dob);
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getHp1() {
		return this.hp1;
	}

	public void setHp1(String hp1) {
		this.hp1 = hp1;
	}

	public String getHp2() {
		return this.hp2;
	}

	public void setHp2(String hp2) {
		this.hp2 = hp2;
	}

	public String getHp3() {
		return this.hp3;
	}

	public void setHp3(String hp3) {
		this.hp3 = hp3;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKeyFlag() {
		return this.keyFlag;
	}

	public void setKeyFlag(String keyFlag) {
		this.keyFlag = keyFlag;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalIdNo() {
		return this.legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getManagement() {
		return this.management;
	}

	public void setManagement(String management) {
		this.management = management;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPob() {
		return this.pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

}