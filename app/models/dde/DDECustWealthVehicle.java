package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUST_WEALTH_VEHICLE database table.
 * 
 */
@Entity
@Table(name="ORIG_CUST_WEALTH_VEHICLE", schema="WISE")
@NamedQuery(name="DDECustWealthVehicle.findAll", query="SELECT d FROM DDECustWealthVehicle d")
public class DDECustWealthVehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	private String manufacturer;

	@Column(name="MANUFACTURING_NATION")
	private String manufacturingNation;

	@Column(name="MARKET_VALUE")
	private BigDecimal marketValue;

	@Column(name="\"TYPE\"")
	private String type;

	@Column(name="YEAR_OF_MANUFACTURE")
	private Timestamp yearOfManufacture;

	public DDECustWealthVehicle() {
	}

	public DDECustWealthVehicle(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getManufacturingNation() {
		return this.manufacturingNation;
	}

	public void setManufacturingNation(String manufacturingNation) {
		this.manufacturingNation = manufacturingNation;
	}

	public BigDecimal getMarketValue() {
		return this.marketValue;
	}

	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getYearOfManufacture() {
		return TimestampTransformer.timestampToString(this.yearOfManufacture);
	}

	public void setYearOfManufacture(String yearOfManufacture) {
		this.yearOfManufacture = TimestampTransformer.stringToTimestamp(yearOfManufacture);
	}

}