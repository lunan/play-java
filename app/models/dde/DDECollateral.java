package models.dde;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ORIG_COLLATERAL database table.
 * 
 */
@Entity
@Table(name="ORIG_COLLATERAL", schema="WISE")
@NamedQuery(name="DDECollateral.findAll", query="SELECT d FROM DDECollateral d")
public class DDECollateral implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="COLLATERAL_TYPE")
	private String collateralType;

	@Column(name="COLLATERAL_VALUE")
	private String collateralValue;

	private String guarantor;

	public DDECollateral() {
	}

	public DDECollateral(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCollateralType() {
		return this.collateralType;
	}

	public void setCollateralType(String collateralType) {
		this.collateralType = collateralType;
	}

	public String getCollateralValue() {
		return this.collateralValue;
	}

	public void setCollateralValue(String collateralValue) {
		this.collateralValue = collateralValue;
	}

	public String getGuarantor() {
		return this.guarantor;
	}

	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}

}