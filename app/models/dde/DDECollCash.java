package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_CASH database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_CASH", schema="WISE")
@NamedQuery(name="DDECollCash.findAll", query="SELECT d FROM DDECollCash d")
public class DDECollCash implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="AS_OF_DT")
	private Timestamp asOfDt;

	@Column(name="BILYET_NO")
	private String bilyetNo;

	@Column(name="CURRENCY_DT")
	private Timestamp currencyDt;

	@Column(name="DUE_DT")
	private Timestamp dueDt;

	@Column(name="NISBAH_BANK")
	private BigDecimal nisbahBank;

	@Column(name="NISBAH_CUSTOMER")
	private BigDecimal nisbahCustomer;

	private String owner;

	@Column(name="SERIAL_NO")
	private String serialNo;

	@Column(name="\"VALUE\"")
	private BigDecimal value;

	public DDECollCash() {
	}

	public DDECollCash(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAsOfDt() {
		return TimestampTransformer.timestampToString(this.asOfDt);
	}

	public void setAsOfDt(String asOfDt) {
		this.asOfDt = TimestampTransformer.stringToTimestamp(asOfDt);
	}

	public String getBilyetNo() {
		return this.bilyetNo;
	}

	public void setBilyetNo(String bilyetNo) {
		this.bilyetNo = bilyetNo;
	}

	public String getCurrencyDt() {
		return TimestampTransformer.timestampToString(this.currencyDt);
	}

	public void setCurrencyDt(String currencyDt) {
		this.currencyDt = TimestampTransformer.stringToTimestamp(currencyDt);
	}

	public String getDueDt() {
		return TimestampTransformer.timestampToString(this.dueDt);
	}

	public void setDueDt(String dueDt) {
		this.dueDt = TimestampTransformer.stringToTimestamp(dueDt);
	}

	public BigDecimal getNisbahBank() {
		return this.nisbahBank;
	}

	public void setNisbahBank(BigDecimal nisbahBank) {
		this.nisbahBank = nisbahBank;
	}

	public BigDecimal getNisbahCustomer() {
		return this.nisbahCustomer;
	}

	public void setNisbahCustomer(BigDecimal nisbahCustomer) {
		this.nisbahCustomer = nisbahCustomer;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}