package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_VEHICLE database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_VEHICLE", schema="WISE")
@NamedQuery(name="DDECollVehicle.findAll", query="SELECT d FROM DDECollVehicle d")
public class DDECollVehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="\"ADDRESS\"")
	private String address;

	@Column(name="BODY_NO")
	private String bodyNo;

	private String bpkb;

	@Column(name="BPKB_ISSUE_DT")
	private Timestamp bpkbIssueDt;

	@Column(name="ENGINE_NO")
	private String engineNo;

	@Column(name="KABUPATEN_KOTA")
	private String kabupatenKota;

	private String kecamatan;

	private String kelurahan;

	private String kodepos;

	@Column(name="LICENSE_PLATE")
	private String licensePlate;

	private String manufacturer;

	private String model;

	private String propinsi;

	private String purpose;

	private int rt;

	private int rw;

	@Column(name="\"STATUS\"")
	private String status;

	@Column(name="\"TYPE\"")
	private String type;

	@Column(name="TYPE_2")
	private String type2;

	public DDECollVehicle() {
	}

	public DDECollVehicle(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBodyNo() {
		return this.bodyNo;
	}

	public void setBodyNo(String bodyNo) {
		this.bodyNo = bodyNo;
	}

	public String getBpkb() {
		return this.bpkb;
	}

	public void setBpkb(String bpkb) {
		this.bpkb = bpkb;
	}

	public String getBpkbIssueDt() {
		return TimestampTransformer.timestampToString(this.bpkbIssueDt);
	}

	public void setBpkbIssueDt(String bpkbIssueDt) {
		this.bpkbIssueDt = TimestampTransformer.stringToTimestamp(bpkbIssueDt);
	}

	public String getEngineNo() {
		return this.engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getKabupatenKota() {
		return this.kabupatenKota;
	}

	public void setKabupatenKota(String kabupatenKota) {
		this.kabupatenKota = kabupatenKota;
	}

	public String getKecamatan() {
		return this.kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return this.kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKodepos() {
		return this.kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getLicensePlate() {
		return this.licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPropinsi() {
		return this.propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public int getRt() {
		return this.rt;
	}

	public void setRt(int rt) {
		this.rt = rt;
	}

	public int getRw() {
		return this.rw;
	}

	public void setRw(int rw) {
		this.rw = rw;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType2() {
		return this.type2;
	}

	public void setType2(String type2) {
		this.type2 = type2;
	}

}