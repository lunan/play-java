package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_FINANCE_BSM_LOAN database table.
 * 
 */
@Entity
@Table(name="ORIG_FINANCE_BSM_LOAN", schema="WISE")
@NamedQuery(name="DDEFinanceBsmLoan.findAll", query="SELECT d FROM DDEFinanceBsmLoan d")
public class DDEFinanceBsmLoan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="BRANCH_CD")
	private String branchCd;

	@Column(name="DUE_DT")
	private Timestamp dueDt;

	private BigDecimal installment;

	private String kolektabilitas;

	@Column(name="LOAN_NO")
	private String loanNo;

	private BigDecimal outstanding;

	private BigDecimal plafond;

	@Column(name="\"USAGE\"")
	private String usage;

	public DDEFinanceBsmLoan() {
	}

	public DDEFinanceBsmLoan(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getBranchCd() {
		return this.branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getDueDt() {
		return TimestampTransformer.timestampToString(this.dueDt);
	}

	public void setDueDt(String dueDt) {
		this.dueDt = TimestampTransformer.stringToTimestamp(dueDt);
	}

	public BigDecimal getInstallment() {
		return this.installment;
	}

	public void setInstallment(BigDecimal installment) {
		this.installment = installment;
	}

	public String getKolektabilitas() {
		return this.kolektabilitas;
	}

	public void setKolektabilitas(String kolektabilitas) {
		this.kolektabilitas = kolektabilitas;
	}

	public String getLoanNo() {
		return this.loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public BigDecimal getOutstanding() {
		return this.outstanding;
	}

	public void setOutstanding(BigDecimal outstanding) {
		this.outstanding = outstanding;
	}

	public BigDecimal getPlafond() {
		return this.plafond;
	}

	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}

	public String getUsage() {
		return this.usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

}