package models.dde;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_WEALTH_PROPERTY database table.
 * 
 */
@Entity
@Table(name="ORIG_WEALTH_PROPERTY", schema="WISE")
@NamedQuery(name="DDEWealthProperty.findAll", query="SELECT d FROM DDEWealthProperty d")
public class DDEWealthProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="BUILDING_AREA_M2")
	private BigDecimal buildingAreaM2;

	@Column(name="LAND_AREA_M2")
	private BigDecimal landAreaM2;

	@Column(name="\"LOCATION\"")
	private String location;

	@Column(name="OWNERSHIP_STATUS")
	private String ownershipStatus;

	private BigDecimal price;

	@Column(name="\"TYPE\"")
	private String type;

	public DDEWealthProperty() {
	}

	public DDEWealthProperty(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getBuildingAreaM2() {
		return this.buildingAreaM2;
	}

	public void setBuildingAreaM2(BigDecimal buildingAreaM2) {
		this.buildingAreaM2 = buildingAreaM2;
	}

	public BigDecimal getLandAreaM2() {
		return this.landAreaM2;
	}

	public void setLandAreaM2(BigDecimal landAreaM2) {
		this.landAreaM2 = landAreaM2;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOwnershipStatus() {
		return this.ownershipStatus;
	}

	public void setOwnershipStatus(String ownershipStatus) {
		this.ownershipStatus = ownershipStatus;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}