package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_WEALTH_ACCT database table.
 * 
 */
@Entity
@Table(name="ORIG_WEALTH_ACCT", schema="WISE")
@NamedQuery(name="DDEWealthAcct.findAll", query="SELECT d FROM DDEWealthAcct d")
public class DDEWealthAcct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="ACCT_NO")
	private String acctNo;

	@Column(name="ACCT_TYPE")
	private String acctType;

	@Column(name="AVG_BAL")
	private BigDecimal avgBal;

	@Column(name="AVG_PURCHACE")
	private BigDecimal avgPurchace;

	@Column(name="AVG_SALES")
	private BigDecimal avgSales;

	private BigDecimal balance;

	@Column(name="BANK_NAME")
	private String bankName;

	private Timestamp since;

	public DDEWealthAcct() {
	}

	public DDEWealthAcct(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAcctNo() {
		return this.acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getAcctType() {
		return this.acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public BigDecimal getAvgBal() {
		return this.avgBal;
	}

	public void setAvgBal(BigDecimal avgBal) {
		this.avgBal = avgBal;
	}

	public BigDecimal getAvgPurchace() {
		return this.avgPurchace;
	}

	public void setAvgPurchace(BigDecimal avgPurchace) {
		this.avgPurchace = avgPurchace;
	}

	public BigDecimal getAvgSales() {
		return this.avgSales;
	}

	public void setAvgSales(BigDecimal avgSales) {
		this.avgSales = avgSales;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getSince() {
		return TimestampTransformer.timestampToString(this.since);
	}

	public void setSince(String since) {
		this.since = TimestampTransformer.stringToTimestamp(since);
	}

}