package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_FINANCE_LOAN database table.
 * 
 */
@Entity
@Table(name="ORIG_FINANCE_LOAN", schema="WISE")
@NamedQuery(name="DDEFinanceLoan.findAll", query="SELECT d FROM DDEFinanceLoan d")
public class DDEFinanceLoan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	private BigDecimal balance;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="DUE_DT")
	private Timestamp dueDt;

	private BigDecimal installment;

	private BigDecimal outstanding;

	@Column(name="\"TYPE\"")
	private String type;

	public DDEFinanceLoan() {
	}

	public DDEFinanceLoan(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getDueDt() {
		return TimestampTransformer.timestampToString(this.dueDt);
	}

	public void setDueDt(String dueDt) {
		this.dueDt = TimestampTransformer.stringToTimestamp(dueDt);
	}

	public BigDecimal getInstallment() {
		return this.installment;
	}

	public void setInstallment(BigDecimal installment) {
		this.installment = installment;
	}

	public BigDecimal getOutstanding() {
		return this.outstanding;
	}

	public void setOutstanding(BigDecimal outstanding) {
		this.outstanding = outstanding;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}