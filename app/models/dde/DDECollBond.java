package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_COLL_BOND database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_BOND", schema="WISE")
@NamedQuery(name="DDECollBond.findAll", query="SELECT d FROM DDECollBond d")
public class DDECollBond implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="CONTRACT_NO")
	private String contractNo;

	private Timestamp dt;

	private String issuer;

	private String tenor;

	private String underlying;

	public DDECollBond() {
	}

	public DDECollBond(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getContractNo() {
		return this.contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getDt() {
		return TimestampTransformer.timestampToString(this.dt);
	}

	public void setDt(String dt) {
		this.dt = TimestampTransformer.stringToTimestamp(dt);
	}

	public String getIssuer() {
		return this.issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getTenor() {
		return this.tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getUnderlying() {
		return this.underlying;
	}

	public void setUnderlying(String underlying) {
		this.underlying = underlying;
	}

}