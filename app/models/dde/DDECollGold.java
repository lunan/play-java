package models.dde;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_COLL_GOLD database table.
 * 
 */
@Entity
@Table(name="ORIG_COLL_GOLD", schema="WISE")
@NamedQuery(name="DDECollGold.findAll", query="SELECT d FROM DDECollGold d")
public class DDECollGold implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="CERTIFICATE_ISSUER")
	private String certificateIssuer;

	@Column(name="CERTIFICATE_NO")
	private String certificateNo;

	private String faktur;

	private BigDecimal karatase;

	@Column(name="PRICE_PER_GRAM")
	private BigDecimal pricePerGram;

	@Column(name="WEIGHT_GRAM")
	private BigDecimal weightGram;

	public DDECollGold() {
	}

	public DDECollGold(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCertificateIssuer() {
		return this.certificateIssuer;
	}

	public void setCertificateIssuer(String certificateIssuer) {
		this.certificateIssuer = certificateIssuer;
	}

	public String getCertificateNo() {
		return this.certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getFaktur() {
		return this.faktur;
	}

	public void setFaktur(String faktur) {
		this.faktur = faktur;
	}

	public BigDecimal getKaratase() {
		return this.karatase;
	}

	public void setKaratase(BigDecimal karatase) {
		this.karatase = karatase;
	}

	public BigDecimal getPricePerGram() {
		return this.pricePerGram;
	}

	public void setPricePerGram(BigDecimal pricePerGram) {
		this.pricePerGram = pricePerGram;
	}

	public BigDecimal getWeightGram() {
		return this.weightGram;
	}

	public void setWeightGram(BigDecimal weightGram) {
		this.weightGram = weightGram;
	}

}