package models.dde;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_UPLOAD_DOCUMENT database table.
 * 
 */
@Entity
@Table(name="ORIG_UPLOAD_DOCUMENT", schema="WISE")
@NamedQuery(name="DDEUploadDocument.findAll", query="SELECT d FROM DDEUploadDocument d")
public class DDEUploadDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="DOCUMENT_EXT")
	private String documentExt;

	@Column(name="DOCUMENT_OWNER")
	private String documentOwner;

	@Column(name="DOCUMENT_PATH")
	private String documentPath;

	@Column(name="DOCUMENT_TYPE")
	private String documentType;

	@Column(name="EXPIRED_DATE")
	private Timestamp expiredDate;

	private String information;

	@Column(name="UPLOAD_DATE")
	private Timestamp uploadDate;

	private String uploaded;

	public DDEUploadDocument() {
	}

	public DDEUploadDocument(String applicationId) {
		super();
		this.applicationId = applicationId;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getDocumentExt() {
		return this.documentExt;
	}

	public void setDocumentExt(String documentExt) {
		this.documentExt = documentExt;
	}

	public String getDocumentOwner() {
		return this.documentOwner;
	}

	public void setDocumentOwner(String documentOwner) {
		this.documentOwner = documentOwner;
	}

	public String getDocumentPath() {
		return this.documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getExpiredDate() {
		return TimestampTransformer.timestampToString(this.expiredDate);
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = TimestampTransformer.stringToTimestamp(expiredDate);
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getUploadDate() {
		return TimestampTransformer.timestampToString(this.uploadDate);
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = TimestampTransformer.stringToTimestamp(uploadDate);
	}

	public String getUploaded() {
		return this.uploaded;
	}

	public void setUploaded(String uploaded) {
		this.uploaded = uploaded;
	}

}