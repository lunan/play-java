package models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the JMS_MESSAGE_STORE database table.
 * 
 */
@Entity
@Table(name="JMS_MESSAGE_STORE",schema="WISE")
@NamedQuery(name="JmsMessageStore.findAll", query="SELECT j FROM JmsMessageStore j")
public class JmsMessageStore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"ID\"")
	private int id;

	private String direction;

	private String message;

	public JmsMessageStore() {
	}
	
	public JmsMessageStore(int id, String message, String direction){
	    this.id=id;
		this.message=message;
		this.direction=direction;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}