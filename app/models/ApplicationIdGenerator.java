package models;

import javax.inject.Singleton;
import javax.persistence.Query;

import play.db.jpa.JPA;

@Singleton
public class ApplicationIdGenerator {
	
	public String getApplicationId(){
		Query appId = JPA.em().createNativeQuery("VALUES NEXT VALUE FOR WISE.ORIG_LOAN_ID");
		return  ((Integer) appId.getSingleResult()).toString();
	}
}
