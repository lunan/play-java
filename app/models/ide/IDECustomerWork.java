package models.ide;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the ORIG_CUSTOMER_WORK database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_WORK", schema="WISE")
@NamedQuery(name="IDECustomerWork.findAll", query="SELECT i FROM IDECustomerWork i")
public class IDECustomerWork implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="CORP_NAME")
	private String corpName;

	@Column(name="TYPE_OF_WORK")
	private String typeOfWork;

	public IDECustomerWork() {
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getTypeOfWork() {
		return typeOfWork;
	}

	public void setTypeOfWork(String typeOfWork) {
		this.typeOfWork = typeOfWork;
	}
}