package models.ide;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_CUSTOMER_INCOME database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_INCOME", schema="WISE")
@NamedQuery(name="IDECustomerIncome.findAll", query="SELECT i FROM IDECustomerIncome i")
public class IDECustomerIncome implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="SPOUSE_INCOME")
	private BigDecimal spouseIncome;
	
	@Column(name="TOTAL_INCOME")
	private BigDecimal totalIncome;

	public IDECustomerIncome() {
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getSpouseIncome() {
		return this.spouseIncome;
	}

	public void setSpouseIncome(BigDecimal spouseIncome) {
		this.spouseIncome = spouseIncome;
	}

	public BigDecimal getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome = totalIncome;
	}

}