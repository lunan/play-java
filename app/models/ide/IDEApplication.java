package models.ide;

import java.io.Serializable;
import javax.persistence.*;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;


/**
 * The persistent class for the ORIG_APPLICATION database table.
 * 
 */
@Entity
@Table(name="ORIG_APPLICATION", schema="WISE")
@NamedQuery(name="IDEApplication.findAll", query="SELECT i FROM IDEApplication i ORDER BY i.id")
public class IDEApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"ID\"")
	private String id;

	private String agency;

	private String barcode;

	@Column(name="BRANCH_CD")
	private String branchCd;

	private String channel;

	private String cif;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="CSM_CD")
	private String csmCd;

	@Column(name="CSM_NM")
	private String csmNm;

	@Column(name="DASAR_USULAN")
	private String dasarUsulan;

	@Column(name="JENIS_PEMOHON")
	private String jenisPemohon;

	@Column(name="MARKETING_CD")
	private String marketingCd;

	@Column(name="MARKETING_NM")
	private String marketingNm;

	@Column(name="PROGRAM_CD")
	private String programCd;

	@Column(name="REQUEST_DATE")
	private Timestamp requestDate;

	private String supervisor;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Column(name="UPDATED_DT")
	private Timestamp updatedDt;

	@Column(name="\"VERSION\"")
	private int version;

	public IDEApplication() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAgency() {
		return this.agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getBranchCd() {
		return this.branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCif() {
		return this.cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCsmCd() {
		return this.csmCd;
	}

	public void setCsmCd(String csmCd) {
		this.csmCd = csmCd;
	}

	public String getCsmNm() {
		return this.csmNm;
	}

	public void setCsmNm(String csmNm) {
		this.csmNm = csmNm;
	}

	public String getDasarUsulan() {
		return this.dasarUsulan;
	}

	public void setDasarUsulan(String dasarUsulan) {
		this.dasarUsulan = dasarUsulan;
	}

	public String getJenisPemohon() {
		return this.jenisPemohon;
	}

	public void setJenisPemohon(String jenisPemohon) {
		this.jenisPemohon = jenisPemohon;
	}

	public String getMarketingCd() {
		return this.marketingCd;
	}

	public void setMarketingCd(String marketingCd) {
		this.marketingCd = marketingCd;
	}

	public String getMarketingNm() {
		return this.marketingNm;
	}

	public void setMarketingNm(String marketingNm) {
		this.marketingNm = marketingNm;
	}

	public String getProgramCd() {
		return this.programCd;
	}

	public void setProgramCd(String programCd) {
		this.programCd = programCd;
	}

	public String getRequestDate() {
		return TimestampTransformer.timestampToString(this.requestDate);
	}

	public void setRequestDate(String requestDate) {
		if(requestDate != null && !requestDate.equals(""))
		{
			this.requestDate = TimestampTransformer.stringToTimestamp(requestDate);
		} else {
			this.requestDate = TimestampTransformer.getNow();
		}
		 
	}

	public String getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDt() {
		return TimestampTransformer.timestampToString(this.updatedDt);
	}

	public void setUpdatedDt(String updatedDt) {
		this.updatedDt = TimestampTransformer.stringToTimestamp(updatedDt);
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}