package models.ide;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import models.transformer.TimestampTransformer;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The persistent class for the ORIG_CUSTOMER_SPOUSE database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER_SPOUSE",schema="WISE")
@NamedQuery(name="IDECustomerSpouse.findAll", query="SELECT o FROM IDECustomerSpouse o")
public class IDECustomerSpouse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	private Timestamp dob;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="FULL_NAME")
	private String fullName;

	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="LEGAL_TYPE")
	private String legalType;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	private String pob;
	
	@Column(name="LEGAL_ID_NO")
	private String legalIdNo;

	public IDECustomerSpouse() {
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getDob() {
		return TimestampTransformer.timestampToString(dob);
	}

	public void setDob(String dob) throws Exception {
		this.dob = TimestampTransformer.stringToTimestamp(dob);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalType() {
		return legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLegalIdNo() {
		return legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	
	

}