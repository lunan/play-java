package models.ide;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_CUST_FINANCE_SUMMARY database table.
 * 
 */
@Entity
@Table(name="ORIG_CUST_FINANCE_SUMMARY", schema="WISE")
@NamedQuery(name="IDECustomerFinanceSummary.findAll", query="SELECT i FROM IDECustomerFinanceSummary i")
public class IDECustomerFinanceSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="TOTAL_LOAN_INSTALLMENT")
	private BigDecimal totalLoanInstallment;

	public IDECustomerFinanceSummary() {
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getTotalLoanInstallment() {
		return this.totalLoanInstallment;
	}

	public void setTotalLoanInstallment(BigDecimal totalLoanInstallment) {
		this.totalLoanInstallment = totalLoanInstallment;
	}

}