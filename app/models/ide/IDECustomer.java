package models.ide;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import models.ApplicationID;
import models.transformer.TimestampTransformer;


/**
 * The persistent class for the ORIG_CUSTOMER database table.
 * 
 */
@Entity
@Table(name="ORIG_CUSTOMER",schema="WISE")
@NamedQuery(name="IDECustomer.findAll", query="SELECT o FROM IDECustomer o")
public class IDECustomer implements Serializable,ApplicationID {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

//	@Column(name="CIF_NO")
//	private String cifNo;
	
	private Timestamp dob;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="FULL_NAME")
	private String fullName;

	private String hp1;

	private String hp2;
	
	private String hp3;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;

	@Column(name="LEGAL_ID_NO", nullable=false)
	private String legalIdNo;

	@Column(name="LEGAL_KABUPATEN_KOTA")
	private String legalKabupatenKota;

	@Column(name="LEGAL_KECAMATAN")
	private String legalKecamatan;

	@Column(name="LEGAL_KELURAHAN")
	private String legalKelurahan;

	@Column(name="LEGAL_KODEPOS")
	private String legalKodepos;

	@Column(name="LEGAL_NAME")
	private String legalName;

	@Column(name="LEGAL_PROPINSI")
	private String legalPropinsi;

	@Column(name="LEGAL_RT")
	private int legalRt;

	@Column(name="LEGAL_RW")
	private int legalRw;

	@Column(name="LEGAL_TYPE")
	private String legalType;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;

	@Column(name="MOTHER_MAIDEN_NAME")
	private String motherMaidenName;

	private String phone;
	
	private String pob;


	public IDECustomer() {
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

//	public String getCifNo() {
//		return cifNo;
//	}
//
//	public void setCifNo(String cifNo) {
//		this.cifNo = cifNo;
//	}

	public String getDob() {
		return TimestampTransformer.timestampToString(dob);
	}

	public void setDob(String dob) throws Exception {
		this.dob = TimestampTransformer.stringToTimestamp(dob);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getHp1() {
		return hp1;
	}

	public void setHp1(String hp1) {
		this.hp1 = hp1;
	}

	public String getHp2() {
		return hp2;
	}

	public void setHp2(String hp2) {
		this.hp2 = hp2;
	}
	
	public String getHp3() {
		return hp3;
	}

	public void setHp3(String hp3) {
		this.hp3 = hp3;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLegalAddress() {
		return legalAddress;
	}

	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	public String getLegalIdNo() {
		return legalIdNo;
	}

	public void setLegalIdNo(String legalIdNo) {
		this.legalIdNo = legalIdNo;
	}

	public String getLegalKabupatenKota() {
		return legalKabupatenKota;
	}

	public void setLegalKabupatenKota(String legalKabupatenKota) {
		this.legalKabupatenKota = legalKabupatenKota;
	}

	public String getLegalKecamatan() {
		return legalKecamatan;
	}

	public void setLegalKecamatan(String legalKecamatan) {
		this.legalKecamatan = legalKecamatan;
	}

	public String getLegalKelurahan() {
		return legalKelurahan;
	}

	public void setLegalKelurahan(String legalKelurahan) {
		this.legalKelurahan = legalKelurahan;
	}

	public String getLegalKodepos() {
		return legalKodepos;
	}

	public void setLegalKodepos(String legalKodepos) {
		this.legalKodepos = legalKodepos;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalPropinsi() {
		return legalPropinsi;
	}

	public void setLegalPropinsi(String legalPropinsi) {
		this.legalPropinsi = legalPropinsi;
	}

	public int getLegalRt() {
		return legalRt;
	}

	public void setLegalRt(int legalRt) {
		this.legalRt = legalRt;
	}

	public int getLegalRw() {
		return legalRw;
	}

	public void setLegalRw(int legalRw) {
		this.legalRw = legalRw;
	}

	public String getLegalType() {
		return legalType;
	}

	public void setLegalType(String legalType) {
		this.legalType = legalType;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

}