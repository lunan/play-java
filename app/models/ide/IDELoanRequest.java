package models.ide;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;


/**
 * The persistent class for the ORIG_LOAN_REQUEST database table.
 * 
 */
@Entity
@Table(name="ORIG_LOAN_REQUEST",schema="WISE")
@NamedQuery(name="IDELoanRequest.findAll", query="SELECT o FROM IDELoanRequest o")
public class IDELoanRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APPLICATION_ID")
	private String applicationId;

	@Column(name="TOTAL_COLL_MARKET_VALUE")
	private BigDecimal totalCollMarketValue;

	@Column(name="COLLATERAL_TYPE")
	private String collateralType;

	@Column(name="DOWN_PAYMENT")
	private BigDecimal downPayment;

	@Column(name="INSTALLMENT_VALUE")
	private String installmentValue;

	@Column(name="LOAN_PURPOSE")
	private String loanPurpose;

	@Column(name="LOAN_TERM")
	private int loanTerm;

	@Column(name="FINANCING_SCHEMA")
	private String financingSchema;

	@Column(name="FINANCING_VALUE")
	private BigDecimal financingValue;

	@Column(name="MARGIN_RATE")
	private BigDecimal marginRate;

	@Column(name="MARGIN_TYPE")
	private String marginType;

	@Column(name="MARGIN_VALUE")
	private BigDecimal marginValue;

	@Column(name="PRODUCT_TYPE")
	private String productType;

	@Column(name="SETTLEMENT_SOURCE")
	private String settlementSource;

	public IDELoanRequest() {
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BigDecimal getTotalCollMarketValue() {
		return totalCollMarketValue;
	}

	public void setTotalCollMarketValue(BigDecimal totalCollMarketValue) {
		this.totalCollMarketValue = totalCollMarketValue;
	}

	public String getCollateralType() {
		return this.collateralType;
	}

	public void setCollateralType(String collateralType) {
		this.collateralType = collateralType;
	}

	public BigDecimal getDownPayment() {
		return this.downPayment;
	}

	public void setDownPayment(BigDecimal downPayment) {
		this.downPayment = downPayment;
	}

	public String getInstallmentValue() {
		return this.installmentValue;
	}

	public void setInstallmentValue(String installmentValue) {
		this.installmentValue = installmentValue;
	}

	public String getLoanPurpose() {
		return this.loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public int getLoanTerm() {
		return this.loanTerm;
	}

	public void setLoanTerm(int loanTerm) {
		this.loanTerm = loanTerm;
	}

	public String getFinancingSchema() {
		return this.financingSchema;
	}

	public void setFinancingSchema(String financingSchema) {
		this.financingSchema = financingSchema;
	}

	public BigDecimal getFinancingValue() {
		return this.financingValue;
	}

	public void setFinancingValue(BigDecimal financingValue) {
		this.financingValue = financingValue;
	}

	public BigDecimal getMarginRate() {
		return this.marginRate;
	}

	public void setMarginRate(BigDecimal marginRate) {
		this.marginRate = marginRate;
	}

	public String getMarginType() {
		return this.marginType;
	}

	public void setMarginType(String marginType) {
		this.marginType = marginType;
	}

	public BigDecimal getMarginValue() {
		return this.marginValue;
	}

	public void setMarginValue(BigDecimal marginValue) {
		this.marginValue = marginValue;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSettlementSource() {
		return this.settlementSource;
	}

	public void setSettlementSource(String settlementSource) {
		this.settlementSource = settlementSource;
	}

}