package services;

import java.util.ArrayList;
import java.util.List;

import models.utl.BSMAddress;
import play.db.jpa.JPA;

public class AddressService {

	public List<?> getAllPropinsi(){
		List<?> listPropinsi = JPA.em().createNativeQuery(BSMAddress.GET_PROPINSI).getResultList();
		return listPropinsi;
	}
	
	public List<?> getKabupatenKota(String propinsi){
		List<?> listKotaKabupaten = JPA.em().createNativeQuery(BSMAddress.GET_KAB_KOTA)
				.setParameter("propinsi", propinsi)
				.getResultList();
		return listKotaKabupaten;
	}
	
	public List<?> getKecamatan(String propinsi, String kotaKabupaten){
		List<?> listKecamatan = JPA.em().createNativeQuery(BSMAddress.GET_KECAMATAN)
				.setParameter("propinsi", propinsi)
				.setParameter("kabupatenKota", kotaKabupaten)
				.getResultList();
		return listKecamatan;
	}
	
	public List<?> getKelurahan(String propinsi, String kotaKabupaten,String kecamatan){
		List<?> listKelurahan = JPA.em().createNativeQuery(BSMAddress.GET_KELURAHAN)
				.setParameter("propinsi", propinsi)
				.setParameter("kabupatenKota", kotaKabupaten)
				.setParameter("kecamatan", kecamatan)
				.getResultList();
		return listKelurahan;
	}
}
