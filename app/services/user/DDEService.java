package services.user;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Singleton;

import models.DDEJSONModel;
import models.JSONModel;
import models.dde.DDEApplication;
import models.dde.DDECollBond;
import models.dde.DDECollCash;
import models.dde.DDECollGold;
import models.dde.DDECollLand;
import models.dde.DDECollLnb;
import models.dde.DDECollMachine;
import models.dde.DDECollSk;
import models.dde.DDECollVehicle;
import models.dde.DDECollateral;
import models.dde.DDECustWealthVehicle;
import models.dde.DDECustomer;
import models.dde.DDECustomerEmergency;
import models.dde.DDECustomerSpouse;
import models.dde.DDECustomerSpouseWork;
import models.dde.DDECustomerWork;
import models.dde.DDEFinanceBsmLoan;
import models.dde.DDEFinanceCc;
import models.dde.DDEFinanceLoan;
import models.dde.DDEKeyPerson;
import models.dde.DDELoanRequest;
import models.dde.DDEUploadDocument;
import models.dde.DDEWealthAcct;
import models.dde.DDEWealthProperty;
import play.db.jpa.JPA;



@Singleton
public class DDEService implements CRUDService {

	@Override
	public void save(JSONModel jsonModel) {
		DDEJSONModel ojm = (DDEJSONModel) jsonModel;
		ojm.copyId(ojm.getDdeApplication().getId());
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().persist(object);
		}
	}

	@Override
	public void update(String id, JSONModel jsonModel) {
		DDEJSONModel ojm = (DDEJSONModel) jsonModel;
		ojm.copyId(ojm.getDdeApplication().getId());
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().merge(object);
		}
	}

	@Override
	public void delete(String id) throws Exception {
		DDEJSONModel ojm = (DDEJSONModel) this.get(id);
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().remove(object);
		}
	}

	@Override
	public JSONModel get(String id) throws Exception {
		DDEJSONModel model = new DDEJSONModel();
		model.setDdeApplication((DDEApplication) this.getEntity("models.dde.DDEApplication", id));
		model.setDdeCollBond((DDECollBond) this.getEntity("models.dde.DDECollBond", id));
		model.setDdeCollCash((DDECollCash) this.getEntity("models.dde.DDECollCash", id));
		model.setDdeCollGold((DDECollGold) this.getEntity("models.dde.DDECollGold", id));
		model.setDdeCollLand((DDECollLand) this.getEntity("models.dde.DDECollLand", id));
		model.setDdeCollLnb((DDECollLnb) this.getEntity("models.dde.DDECollLnb", id));
		model.setDdeCollMachine((DDECollMachine) this.getEntity("models.dde.DDECollMachine", id));
		model.setDdeCollSk((DDECollSk) this.getEntity("models.dde.DDECollSk", id));
		model.setDdeCollVehicle((DDECollVehicle) this.getEntity("models.dde.DDECollVehicle", id));
		model.setDdeCollateral((DDECollateral) this.getEntity("models.dde.DDECollateral", id));
		model.setDdeCustWealthVehicle((DDECustWealthVehicle) this.getEntity("models.dde.DDECustWealthVehicle", id));
		model.setDdeCustomer((DDECustomer) this.getEntity("models.dde.DDECustomer", id));
		model.setDdeCustomerEmergency((DDECustomerEmergency) this.getEntity("models.dde.DDECustomerEmergency", id));
		model.setDdeCustomerSpouse((DDECustomerSpouse) this.getEntity("models.dde.DDECustomerSpouse", id));
		model.setDdeCustomerSpouseWork((DDECustomerSpouseWork) this.getEntity("models.dde.DDECustomerSpouseWork", id));
		model.setDdeCustomerWork((DDECustomerWork) this.getEntity("models.dde.DDECustomerWork", id));
		model.setDdeFinanceBsmLoan((DDEFinanceBsmLoan) this.getEntity("models.dde.DDEFinanceBsmLoan", id));
		model.setDdeFinanceCc((DDEFinanceCc) this.getEntity("models.dde.DDEFinanceCc", id));
		model.setDdeFinanceLoan((DDEFinanceLoan) this.getEntity("models.dde.DDEFinanceLoan", id));
		model.setDdeKeyPerson((DDEKeyPerson) this.getEntity("models.dde.DDEKeyPerson", id));
		model.setDdeLoanRequest((DDELoanRequest) this.getEntity("models.dde.DDELoanRequest", id));
		model.setDdeUploadDocument((DDEUploadDocument) this.getEntity("models.dde.DDEUploadDocument", id));
		model.setDdeWealthAcct((DDEWealthAcct) this.getEntity("models.dde.DDEWealthAcct", id));
		model.setDdeWealthProperty((DDEWealthProperty) this.getEntity("models.dde.DDEWealthProperty", id));
		return model;
	}

	@Override
	public JSONModel getBy(String key, String value) {
		return null;

	}

	@Override
	public JSONModel getByMultiple(Map<String, String> keyValue) {
		return null;

	}

	@Override
	public JSONModel getSkeleton() {
		return DDEJSONModel.getInstance();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getall() {
		List<String> allApplication = new ArrayList<>();
		Consumer<DDEApplication> consumer = (DDEApplication app) -> allApplication.add(app.getId());
		JPA.em()
			.createNamedQuery("DDEApplication.findAll")
			.getResultList()
			.iterator()
			.forEachRemaining(consumer);
		return allApplication;
	}

	private Object getEntity(String className, String id) throws Exception{
		try {
			Class<?> c = Class.forName(className);
			Object object = JPA.em().find(c, id);
			if(object == null) {
				Constructor<?> cons = c.getConstructor(String.class);
				return cons.newInstance(id);
			} else {
				return object;
			}
		} catch (Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	

}
