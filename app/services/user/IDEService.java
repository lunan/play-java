package services.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Singleton;

import models.JSONModel;
import models.IDEJSONModel;
import models.ide.IDEApplication;
import models.ide.IDECustomer;
import models.ide.IDECustomerFinanceSummary;
import models.ide.IDECustomerIncome;
import models.ide.IDECustomerSpouse;
import models.ide.IDECustomerWork;
import models.ide.IDELoanRequest;
import play.db.jpa.JPA;

@Singleton
public class IDEService implements CRUDService {

	@Override
	public void save(JSONModel jsonModel) {
		IDEJSONModel ojm = (IDEJSONModel) jsonModel;
		ojm.copyId(ojm.getIdeApplication().getId());
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().persist(object);
		}
	}

	@Override
	public void update(String id, JSONModel jsonModel) {
		IDEJSONModel ojm = (IDEJSONModel) jsonModel;
		ojm.copyId(ojm.getIdeApplication().getId());
		
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().merge(object);
		}
	}

	@Override
	public void delete(String id) {
		IDEJSONModel ojm = (IDEJSONModel) this.get(id);
		List<?> allField = ojm.allFields();
		for (Object object : allField) {
			JPA.em().remove(object);
		}
	}

	@Override
	public JSONModel get(String id) {
		IDEApplication ideApplication = JPA.em().find(IDEApplication.class, id);
		IDECustomer ideCustomer = JPA.em().find(IDECustomer.class, id);
		IDECustomerSpouse ideCustomerSpouse = JPA.em().find(IDECustomerSpouse.class, id);
		IDELoanRequest ideLoanRequest = JPA.em().find(IDELoanRequest.class, id);
		IDECustomerIncome ideCustomerIncome = JPA.em().find(IDECustomerIncome.class, id);
		IDECustomerWork ideCustomerWork = JPA.em().find(IDECustomerWork.class, id);
		IDECustomerFinanceSummary ideCustomerFinanceSummary = JPA.em().find(IDECustomerFinanceSummary.class, id);
		JSONModel model = new IDEJSONModel(ideApplication, ideCustomer, ideCustomerSpouse, ideLoanRequest, ideCustomerIncome, ideCustomerFinanceSummary, ideCustomerWork); 
		return model;
	}

	@Override
	public JSONModel getBy(String key, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONModel getByMultiple(Map<String, String> keyValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONModel getSkeleton() {
		return new IDEJSONModel(new IDEApplication(), new IDECustomer(), 
				new IDECustomerSpouse(), new IDELoanRequest(), new IDECustomerIncome(),
				new IDECustomerFinanceSummary(), new IDECustomerWork() );
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getall() {
		List<String> allApplication = new ArrayList<>();
		Consumer<IDEApplication> consumer = (IDEApplication app) -> allApplication.add(app.getId());
		JPA.em()
			.createNamedQuery("IDEApplication.findAll")
			.getResultList()
			.iterator()
			.forEachRemaining(consumer);
		return allApplication;
	}

}
