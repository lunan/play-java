package services.user;

import java.util.List;
import java.util.Map;

import models.JSONModel;

public interface CRUDService {
	void save(JSONModel jsonModel);
	void update(String id, JSONModel jsonModel);
	void delete(String id) throws Exception;
	List<String> getall();
	JSONModel get(String id) throws Exception;
	JSONModel getBy(String key, String value);
	JSONModel getByMultiple(Map<String,String> keyValue);
	JSONModel getSkeleton();
}
