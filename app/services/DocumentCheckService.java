package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.doc_check.DocumentCheckList;
import play.db.jpa.JPA;

public class DocumentCheckService {
	
	public List<?> getCategories(){
		List<?> categories = JPA.em().createNativeQuery(DocumentCheckList.GET_CATEGORY).getResultList();
		return this.toListId(categories);
	}
	
	public List<?> getSubCategories(String category){
		List<?> subCategories = JPA.em().createNativeQuery(DocumentCheckList.GET_SUB_CATEGORY)
				.setParameter("category", category)
				.getResultList();
		return this.toListId(subCategories);
	}
	
	public List<?> getDocNames(String category, String subCategory){
		List<?> docNames = JPA.em().createNativeQuery(DocumentCheckList.GET_DOC_NAME)
				.setParameter("category", category)
				.setParameter("subCategory", subCategory)
				.getResultList();
		return this.toListIdWithValue(docNames);
	}
	
	public List<?> toListId(List<?> jdbcList){
		List<Map<String,String>> returnList = new ArrayList<>(); 
		for (Object value : jdbcList) {
			Map<String,String> map = new HashMap<>();
			map.put("id", (String) value);
			returnList.add(map);
		}
		return returnList;
	}
	
	public List<?> toListIdWithValue(List<?> jdbcList){
		List<Map<String,Object>> returnList = new ArrayList<>(); 
		for (Object object : jdbcList) {
			Object[] arrObjects = (Object[]) object;
			Map<String, Object> map = new HashMap<>();
			map.put("id", arrObjects[0]);
			map.put("name", (String) arrObjects[1]);
			map.put("value", (String) arrObjects[2]);
			returnList.add(map);
		}
		return returnList;
	}
}
