package services;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import dao.JmsMessageStoreDao;
import models.JmsMessageStore;

import org.apache.activemq.ActiveMQConnectionFactory;

@Singleton
public class ActiveMQProducerImpl implements ActiveMQProducer {
	
    private final JmsMessageStoreDao messageStore;
	private final ActiveMQConnectionFactory factory;  
	private final Connection connection;
	private final Session session;
	private final Queue queue;
	private final MessageProducer messageProducer;

    @Inject
	public ActiveMQProducerImpl(JmsMessageStoreDao messageStore) throws JMSException {
	    this.messageStore = messageStore;
		factory = new ActiveMQConnectionFactory("tcp://10.2.109.93:61616");
		connection = factory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		queue =  session.createQueue("PLAY");
		messageProducer = session.createProducer(queue);
	}
	
	@Override
	public void Send(String message) throws Exception {
		TextMessage textMessage = session.createTextMessage(message);
		messageProducer.send(textMessage);
		messageStore.saveMessage(new JmsMessageStore(1,message, "OUTGOING"));
	}
}
