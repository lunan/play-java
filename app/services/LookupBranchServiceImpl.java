package services;

import javax.inject.Inject;
import javax.inject.Singleton;

import dao.LookupBranchDao;
import models.LookupBranch;

@Singleton
public class LookupBranchServiceImpl implements LookupBranchService {
	
	private final LookupBranchDao branchDao;
	
	@Inject
	public LookupBranchServiceImpl(LookupBranchDao branchDao) {
		this.branchDao=branchDao;
	}

	@Override
	public LookupBranch find(String branchCd) {
		return branchDao.find(branchCd);
	}

}
