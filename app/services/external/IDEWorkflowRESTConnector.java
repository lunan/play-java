package services.external;

import javax.inject.Inject;

import models.IDEJSONModel;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;

public class IDEWorkflowRESTConnector {
	
	private static final String INIT_URL = "http://10.12.12.18:15000/act/task/ide/userid/IDE_USER/applicationid/";
	@Inject WSClient ws;
	
	public void init(IDEJSONModel newModel){
		WSRequest wsReq = ws.url(INIT_URL
				+newModel.getIdeApplication().getId()
				+"/npwp/"+newModel.getIdeCustomer().getLegalIdNo());
		wsReq.post("");
		//CompletionStage<WSResponse> resposePromise = wsReq.post("");
	}
}
