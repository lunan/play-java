package services;

import models.LookupBranch;

public interface LookupBranchService {
	LookupBranch find(String branchCd);
}
