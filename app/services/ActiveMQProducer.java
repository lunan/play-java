package services;

public interface ActiveMQProducer {
	void Send(String message) throws Exception;
}
