package services;

import javax.jms.JMSException;

public interface ActiveMQConsumer {
	String ConsumeMessage() throws JMSException;
}
