package services;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import dao.JmsMessageStoreDao;
import models.JmsMessageStore;
import play.db.jpa.Transactional;
import play.db.jpa.JPA;

@Singleton
public class ActiveMQConsumerImpl implements ActiveMQConsumer, MessageListener {

	private final JmsMessageStoreDao messageStore;
	private final ActiveMQConnectionFactory factory;  
	private final Connection connection;
	private final Session session;
	private final Queue queue;
	private final MessageConsumer messageConsumer;
	private final int MESSAGE_TO_CONSUME=1;
	private static int messages = 10;
	
	@Inject
	public ActiveMQConsumerImpl(JmsMessageStoreDao messageStore) throws JMSException {
		this.messageStore = messageStore;
		factory = new ActiveMQConnectionFactory("tcp://10.2.109.93:61616");
		connection = factory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		queue =  session.createQueue("PLAY");
		messageConsumer = session.createConsumer(queue);
		messageConsumer.setMessageListener(this);
	}
	
	@Override
	public String ConsumeMessage() throws JMSException {
		TextMessage textMessage = null;
		//while (MESSAGE_TO_CONSUME > messages) {
			textMessage = (TextMessage) messageConsumer.receive();
			messageStore.saveMessage(new JmsMessageStore(messages,textMessage.getText(), "INCOMING"));
			messages++;
		//}
		return textMessage.getText();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onMessage(Message message) {
	    JPA.withTransaction(() -> {
            TextMessage textMessage = (TextMessage) message;
			try {
				messageStore.saveMessage(new JmsMessageStore(messages+100,textMessage.getText(), "INCOMING"));
			} catch (JMSException e) {	
				e.printStackTrace();
				messageStore.saveMessage(new JmsMessageStore(messages+1000,"ERROR", "INCOMING"));
			}
			messages++;
        });
	}

}
