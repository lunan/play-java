package dao;

import models.LookupBranch;

public interface LookupBranchDao {
    LookupBranch find(String branchCd);
}