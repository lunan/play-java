package dao;

import javax.inject.Singleton;

import models.JmsMessageStore;
import play.db.jpa.JPA;

@Singleton
public class JmsMessageStoreDaoImpl implements JmsMessageStoreDao {

	@Override
	public void saveMessage(JmsMessageStore messageStore) {
		JPA.em().persist(messageStore);
	}
}
