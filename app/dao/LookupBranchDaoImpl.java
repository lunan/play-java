package dao;

import play.db.jpa.JPA;

import javax.inject.Singleton;

import models.LookupBranch;

@Singleton
public class LookupBranchDaoImpl implements LookupBranchDao {
    
    
    public LookupBranch find(String branchCd) {
        //return JPA.em.().find(LookupBranch.class,branchCd);
    	return JPA.em().find(LookupBranch.class, branchCd);
        //return null;
    }
}