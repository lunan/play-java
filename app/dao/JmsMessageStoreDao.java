package dao;

import models.JmsMessageStore;

public interface JmsMessageStoreDao {
	void saveMessage(JmsMessageStore messageStore);
}
