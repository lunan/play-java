package filters;

import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.function.Function;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.auth0.jwt.JWTVerifier;

import akka.stream.Materializer;
import play.mvc.Filter;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;


/**
 * This is a simple filter that adds a header to all requests. It's
 * added to the application's list of filters by the
 * {@link ExampleFilters} class.
 */
@Singleton
public class JWTFilter extends Filter {
	
	//JWT Configuration
	private final String secret = "this is my secret";

    private final Executor exec;

    /**
     * @param mat This object is needed to handle streaming of requests
     * and responses.
     * @param exec This class is needed to execute code asynchronously.
     * It is used below by the <code>thenAsyncApply</code> method.
     */
    @Inject
    public JWTFilter(Materializer mat, Executor exec) {
        super(mat);
        this.exec = exec;
    }

    @Override
    public CompletionStage<Result> apply(
        Function<RequestHeader, CompletionStage<Result>> next,
        RequestHeader requestHeader) {
    	
        return next.apply(requestHeader).thenApplyAsync(
            result -> 
            verifyHeader(requestHeader, result)
            ,
            exec
        );
    }
    
    private Result verifyHeader(RequestHeader requestHeader, Result result){
    	String authHeader = requestHeader.getHeader("Authorization");
    	if(authHeader != null){
    		JWTVerifier verifier = new JWTVerifier(secret);
        	Map<String, Object> claims;
        	try {
        		claims = verifier.verify(authHeader);
        		if(claims != null){
        			return result.withHeader("Authorization-BSM", "true");
        		} else {
        			return play.mvc.Results.unauthorized();
        		}
        	} catch (Exception e){
        		e.printStackTrace();
        		return play.mvc.Results.unauthorized();
        	}
    	} else {
    		return play.mvc.Results.unauthorized();
    	}
    }
    
    

}
