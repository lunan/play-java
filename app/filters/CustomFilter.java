package filters;
import javax.inject.Inject;

import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;

public class CustomFilter extends DefaultHttpFilters {

	@Inject
	public CustomFilter(CORSFilter  corsFilter) {
		super(corsFilter);
	}
}
